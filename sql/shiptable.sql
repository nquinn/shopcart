-- phpMyAdmin SQL Dump
-- version 2.8.2.4
-- http://www.phpmyadmin.net
-- 
-- Host: localhost:3306
-- Generation Time: Nov 01, 2013 at 09:17 PM
-- Server version: 5.0.27
-- PHP Version: 5.2.6
-- 
-- Database: `nocorrosion_com_-_main`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `shiptable`
-- 

CREATE TABLE `shiptable` (
  `minweight` int(4) NOT NULL default '0',
  `maxweight` int(4) NOT NULL default '0',
  `shipcost` int(4) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `shiptable`
-- 

INSERT INTO `shiptable` (`minweight`, `maxweight`, `shipcost`) VALUES (0, 1, 6),
(1, 2, 8),
(2, 6, 10),
(6, 15, 13);
