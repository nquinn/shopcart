<?php

// database connection

function get_mysqli_resource() {

	static $mysqli;

	if(!isset($mysqli)){
	$mysqli = new mysqli("localhost", "root", "root", "shoppingcart");
	// test:  $mysqli = new mysqli("localhost", "testuser", "testdb", "nocorrosion_com_-_test");
	// test:  $mysqli = new mysqli("localhost", "nocorrosion", "upgradethis2@", "nocorrosion_com_-_main");
	}

	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	return $mysqli;
}


?>