<?php

namespace models;

class Product
{
    /**
     * @var int
     */
    private $product_id;
    /**
     * @var string
     */
    private $model;
    /**
     * @var int
     */
    private $price;
    /**
     * @var string
     */
    private $prodinfo;
    /**
     * @var string
     */
    private $url_slug;

    private $is_purchasable;

    private $primary_image_path;
    /* how do we pull an array in here for multiple images? */
    private $image_array;
    
    private function sessionSet($session_key) {
        if (isset($_SESSION[$session_key])) {
            return $_SESSION[$session_key];
        }
        else { return null; }
    }

    /**
     * @param int $product_id
     * @return int $product_id
     */
    public function getId()
    {
        return $this->product_id;
    }
    
    /**
     * @param int $product_id
     * @return int $product_id
     */
    public function setId($product_id)
    {
        $this->product_id = (int) $product_id;
    }

    /**
     * @param string $model
     * @return string $model
     */


    public function getModel()
    {
        return $this->model;
    }
    
    /**
     * @param string $model
     * @return string $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    public function setUrl_slug($url_slug)
    {
        $this->url_slug = $url_slug;
    }

    public function getUrl_slug()
    {
        return $this->url_slug;
    }
    
     /**
     * @param int $price
     * @return int $price
     */
    public function getPrice()
    {
        return $this->price;
    }
    
    /**
     * @param int $price
     * @return int $price
     */
    public function setPrice($price)
    {
        $this->price = (int) $price;
    }
    /**
     * @param string $prodinfo
     * @return string $prodinfo
     */
    public function getProdInfo()
    {
        return $this->prodinfo;
    }
    
    /**
     * @param string $prodinfo
     * @return string $prodinfo
     */
    public function setProdInfo($prodinfo)
    {
        $this->prodinfo = $prodinfo;
    }

    public function getIsPurchasable()
    {
            return $this->is_purchasable;
    }

    public function setIsPurchasable($is_purchasable){
        $this->is_purchasable = $is_purchasable;
    }

    /**
     * @param string $primary_image_path
     * @return string $primary_image_path
     */
    public function getPrimaryImagePath()
    {
        return $this->primary_image_path;
    }
    
    /**
     * @param string $primary_image_path
     * @return string $primary_image_path
     */
    public function setPrimaryImagePath($primary_image_path)
    {
        $this->primary_image_path = $primary_image_path;
        return $this;
    }
    public function getImageArray()
    {
        return $this->image_array;
    }
    public function setImageArray($image_array)
    {
        $this->image_array = $image_array;
    }
}
