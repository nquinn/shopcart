<?php

namespace models;

class Format {
	
	public function formatCurrency($input) {
		$formatted = "$".number_format($input/100, 2);
		return $formatted;
	}

	public function formatDollars($input) { // normally in cents, this is dollars but no $ sign
		$formatted = number_format($input/100, 2);
		return $formatted;
	}
}