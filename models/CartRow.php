<?php

namespace models;

Class CartRow {
	
	private $product_id;
	private $model;
	private $qty;
	private $price;
	private $weight;
	private $line_total;
	private $discount;

	public function getProduct_id(){
		return $this->product_id;
	}

	public function setProduct_id($product_id){
		$this->product_id = (int) $product_id;
	}

	public function getModel(){
		return $this->model;
	}

	public function setModel($model){
		$this->model = $model;
	}

	public function setPrice($price){
		$this->price = $price;
	}

	public function getPrice(){
		return $this->price;
	}

	public function setWeight($weight){
		$this->weight = $weight;
	}

	public function getWeight(){
		return $this->weight;
	}

	public function getQty(){
		return $this->qty;
	}

	public function setQty($qty){
		$this->qty = (int) $qty;
	}

	public function setDiscount($discount) {
		$this->discount = $discount;
	}

	public function getDiscount() {
		return $this->discount;
	}

	public function getLine_total(){
		return $this->line_total;
	}

	public function setLine_total($line_total){
		$this->line_total = $line_total;
	}

	
}