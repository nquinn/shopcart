<?php

namespace models;
use PDO;

class UserRepository {
	
	private $Pdo;
    
    public function __construct(PDO $Pdo)
    {
        $this->Pdo = $Pdo;
    }

    public function authenticateUser($contact_email, $password) {

    	$Stm = $this->Pdo->prepare('
			SELECT customer_id, contact_name, contact_email, password_hash, auth_level from customers
			WHERE contact_email = :contact_email
			');

		$Stm->bindParam(':contact_email',$contact_email,PDO::PARAM_STR);
		$Stm->execute();

		$row = $Stm->fetch(PDO::FETCH_ASSOC);
        if (password_verify($password,$row['password_hash'])) {
          $User = $this->userToObject($row); // get basic customer object
          $customer_id = $row['customer_id'];
          echo"customer id: $customer_id";
          $address_array = $this->getNewestAddresses($customer_id); // get newest addresses and update object with them
          //echo"<pre>".var_dump($address_array)."</pre>";
          $User->setShipAddress_id($address_array['shipping_address_id']);
          $User->setBillAddress_id($address_array['billing_address_id']);
          $User->setZip($address_array['zip']);
          return $User;
        }
        else return null;

    }

    public function userToObject($array) {
    	$User = new \models\User();
    	$User->setCustomer_id($array['customer_id']);
    	$User->setContact_name($array['contact_name']);
    	$User->setAuth_level($array['auth_level']);
    	return $User;
    }

    public function getNewestAddresses($customer_id) {


        $Stmb = $this->Pdo->prepare('
            SELECT address_id
            FROM customers_addresses
            WHERE customer_id = :customer_id
            AND address_type = :address_type
            ORDER by address_id DESC
            LIMIT 1        
            ');

        $addr1_type = 'b';
        $Stmb->bindParam(':customer_id',$customer_id,PDO::PARAM_INT);
        $Stmb->bindParam(':address_type',$addr1_type,PDO::PARAM_STR);
        $Stmb->execute();
        $row = $Stmb->fetch(PDO::FETCH_ASSOC);
        $billing_address_id = $row['address_id'];

        $Stms = $this->Pdo->prepare('
            SELECT address_id, zip
            FROM customers_addresses
            WHERE customer_id = :customer_id2
            AND address_type = :address_type2
            ORDER by address_id DESC
            LIMIT 1        
            ');
        
        $addr2_type = 's';
        $Stms->bindParam(':customer_id2',$customer_id,PDO::PARAM_INT);
        $Stms->bindParam(':address_type2',$addr2_type,PDO::PARAM_STR);
        $Stms->execute();
        $rowb = $Stms->fetch(PDO::FETCH_ASSOC);
        $shipping_address_id = $rowb['address_id'];
        $zip = $rowb['zip'];

    return array(
        'billing_address_id' => $billing_address_id,
        'shipping_address_id' => $shipping_address_id,
        'zip' => $zip
        );

    }
}