<?php

namespace models;
use PDO;

Class OrderRepository {

	private $Pdo;
    
    public function __construct(PDO $Pdo)
    {
        $this->Pdo = $Pdo;
    }

    public function loadOrderItems($order_id) {

    	$Stm = $this->Pdo->prepare('
        SELECT od.product_id, od.qty, od.price, p.model
        FROM order_detail od, products p
        WHERE od.order_id = :order_id
		AND od.product_id = p.product_id
		');
    	
    	$Stm->bindParam(':order_id',$order_id,PDO::PARAM_INT);
    	$Stm->execute();

    	$resultset = $Stm->fetchall(PDO::FETCH_ASSOC);

        $cartRowArray = array();
        $subtotal = 0;

		foreach ($resultset as $productrow) {
			
			$currentid = $productrow['product_id'];
			$currentprice = $productrow['price'];
			$currentqty = $productrow['qty'];
			$currentmodel = $productrow['model'];
			$linetotal = $currentprice*$currentqty;

			$subtotal = $subtotal + $linetotal;

			$CartRow = new CartRow();	
			$CartRow->setProduct_Id($currentid);
			$CartRow->setModel($currentmodel);
			$CartRow->setPrice($currentprice);
			$CartRow->setQty($currentqty);
			$CartRow->setLine_Total($linetotal);

			$cartRowArray[] = $CartRow;

		}

		return $cartRowArray;
        //return $this->arrayToObject($Stm->fetch(PDO::FETCH_ASSOC));

    }

    public function loadOrder($order_id) { // shortcut function to load summary + addresses

        $ship_address = $this->loadOrderShipAddress($order_id); // this is an object
        $bill_address = $this->loadOrderBillAddress($order_id); // this is an object
        $order_summary = $this->loadOrderSummary($order_id); // object also

        $order_summary->setShipping_address($ship_address);  // I update the order_summary obecjt
        $order_summary->setBilling_address($bill_address);
        
        return $order_summary; // object of objects 
    }

    public function loadOrderSummary($order_id){
    	$Stm = $this->Pdo->prepare('
            SELECT 
            c.contact_name, c.contact_email, c.contact_phone, f.order_id, f.order_date, f.track, f.refer,
            f.campaign, f.keyword, f.trans_id, f.auth_code, f.response_code, f.subtotal, f.tax, f.shipping, 
            f.total, f.customer_id
            FROM final_order f, customers c
			WHERE f.order_id = :order_id
            AND f.customer_id = c.customer_id			
        ');
    	
    	$Stm->bindParam(':order_id',$order_id,PDO::PARAM_INT);
    	$Stm->execute();
        $order_summary = $Stm->fetch(PDO::FETCH_ASSOC);
        return $this->orderToObject($order_summary);

    }

    public function loadAddress($address_id) { // load address by addressid only
        $Stm = $this->Pdo->prepare('
            SELECT 
            first, last, company, address1, address2, city, state, zip
            FROM customers_addresses
            WHERE address_id = :address_id
        ');
        
        $Stm->bindParam(':address_id',$address_id,PDO::PARAM_INT);
        $Stm->execute();
        $myaddress = $Stm->fetch(PDO::FETCH_ASSOC);
        
        return $this->addressToObject($myaddress);
    }

    public function loadOrderShipAddress($order_id) { // load address by order id
        $Stm = $this->Pdo->prepare('
            SELECT 
            first, last, company, address1, address2, city, state, zip
            FROM final_order f, customers_addresses ca
            WHERE f.order_id = :order_id
            AND f.ship_address_id = ca.address_id
        ');
        
        $Stm->bindParam(':order_id',$order_id,PDO::PARAM_INT);
        $Stm->execute();
        $ship_address = $Stm->fetch(PDO::FETCH_ASSOC);
        
        return $this->addressToObject($ship_address);
    }

    public function loadOrderBillAddress($order_id) { // load address by order id
        $Stm = $this->Pdo->prepare('
            SELECT 
            first, last, company, address1, address2, city, state, zip
            FROM final_order f, customers_addresses ca
            WHERE f.order_id = :order_id
            AND f.bill_address_id = ca.address_id
        ');
        
        $Stm->bindParam(':order_id',$order_id,PDO::PARAM_INT);
        $Stm->execute();
        $bill_address = $Stm->fetch(PDO::FETCH_ASSOC);
        return $this->addressToObject($bill_address);
    }

    public function loadBillingAddresses($customer_id) {
        // loads all billing addresses ever used by a customer (not for a single order)
        
        $Stmz = $this->Pdo->prepare('
            SELECT 
            address_id, first, last, company, address1, address2, city, state, zip
            FROM customers_addresses
            WHERE customer_id = :customer_id
            AND address_type = :address_type
        ');
        
        $address_type = 'b';

        $Stmz->bindParam(':customer_id',$customer_id,PDO::PARAM_INT);
        $Stmz->bindParam(':address_type',$address_type,PDO::PARAM_STR);
        $Stmz->execute();

        $bill_address_array = array();

        while ($row = $Stmz->fetch(PDO::FETCH_ASSOC)) {
            $Address = $this->addressToObject($row);
            $bill_address_array[] = $Address;
        }

        return $bill_address_array; // array of address objects
    }

    public function loadShippingAddresses($customer_id) {
        // loads all shipping addresses ever used by a customer (not for a single order)
        
        $Stm = $this->Pdo->prepare('
            SELECT 
            address_id, first, last, company, address1, address2, city, state, zip
            FROM customers_addresses
            WHERE customer_id = :customer_id
            AND address_type = :address_type
        ');
        
        $address_type = 's';

        $Stm->bindParam(':customer_id',$customer_id,PDO::PARAM_INT);
        $Stm->bindParam(':address_type',$address_type,PDO::PARAM_STR);
        $Stm->execute();
        //$ship_address = $Stm->fetch(PDO::FETCH_ASSOC);
        
        $ship_address_array = array();

        while ($row = $Stm->fetch(PDO::FETCH_ASSOC)) {
            $Address = $this->addressToObject($row);
            $ship_address_array[] = $Address;
        }

        return $ship_address_array; // array of address objects
    }

    private function addressToObject($array) {
    
        $Address = new Address();

        $Address->setFirst($array['first']);
        $Address->setLast($array['last']);
        $Address->setCompany($array['company']);
        $Address->setAddress1($array['address1']);
        $Address->setAddress2($array['address2']);
        $Address->setCity($array['city']);
        $Address->setState($array['state']);
        $Address->setZip($array['zip']);

        if(isset($array['address_id'])) { $Address->setAddress_Id($array['address_id']); }

        return $Address;

    }

    private function orderToObject($order_summary) {

        $OrderSummary = new OrderSummary();

        $OrderSummary->setContact_name($order_summary['contact_name']);
        $OrderSummary->setContact_email($order_summary['contact_email']);
        $OrderSummary->setContact_phone($order_summary['contact_phone']);
        $OrderSummary->setOrder_id($order_summary['order_id']);
        $OrderSummary->setCustomer_id($order_summary['customer_id']);
        $OrderSummary->setOrder_date($order_summary['order_date']);
        $OrderSummary->setTrack($order_summary['track']);
        $OrderSummary->setRefer($order_summary['refer']);
        $OrderSummary->setCampaign($order_summary['campaign']);
        $OrderSummary->setKeyword($order_summary['keyword']);
        $OrderSummary->setTrans_id($order_summary['trans_id']);
        $OrderSummary->setAuth_code($order_summary['auth_code']);
        $OrderSummary->setResponse_code($order_summary['response_code']);
        $OrderSummary->setSubtotal($order_summary['subtotal']);
        $OrderSummary->setTax($order_summary['tax']);
        $OrderSummary->setShipping($order_summary['shipping']);
        $OrderSummary->setTotal($order_summary['total']);
      
        return $OrderSummary;
    }

    public function saveMerchantResponse($order_id,$trans_id,$auth_code,$response_code) {

    	$Stm = $this->Pdo->prepare('
            UPDATE final_order
            SET trans_id = :trans_id, auth_code = :auth_code, response_code = :response_code
			WHERE order_id = :order_id			
        ');
    	
    	$Stm->bindParam(':trans_id',$trans_id,PDO::PARAM_STR);
    	$Stm->bindParam(':auth_code',$order_id,PDO::PARAM_STR);
    	$Stm->bindParam(':response_code',$order_id,PDO::PARAM_STR);
		$Stm->bindParam(':order_id',$order_id,PDO::PARAM_INT);
    	$Stm->execute();

    }

    public function saveOrderStatus($order_id,$track) {

    	 $Stm = $this->Pdo->prepare('
            UPDATE final_order
            SET track = :track
			WHERE order_id = :order_id			
        ');
    	
    	$Stm->bindParam(':track',$track,PDO::PARAM_STR);
    	$Stm->bindParam(':order_id',$order_id,PDO::PARAM_INT);
    	return $Stm->execute();
    }

    public function listOrders() {
        
        $Stm = $this->Pdo->prepare('
            SELECT order_id, order_date, track, total
            FROM final_order
            ORDER BY order_id DESC
            LIMIT :limit;       
        ');
        
        $limit = 20;

        $Stm->bindParam(':limit',$limit,PDO::PARAM_INT);
        $Stm->execute();

        $resultset = $Stm->fetchall(PDO::FETCH_ASSOC);

        $orderListArray = array();

        foreach ($resultset as $orderrow) {

        $OrderObject = new OrderSummary();
        $OrderObject->setOrder_id($orderrow['order_id']);
        $OrderObject->setOrder_date($orderrow['order_date']);
        $OrderObject->setTotal($orderrow['total']);
        $OrderObject->setTrack($orderrow['track']);

        $orderListArray[] = $OrderObject; // stick each object into the array 

        }
        return $orderListArray;
    }

}