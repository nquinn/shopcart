<?php

namespace models;

class User {

	
    private function sessionSet($session_key) {
    	if (isset($_SESSION[$session_key])) {
    		
    		return $_SESSION[$session_key];
    	}
    	else { return null; }
    }

	public function getCustomer_id(){
		return $this->sessionSet('customer_id');
	}

	public function setCustomer_id($customer_id) {
		$_SESSION['customer_id'] = $customer_id;
	}

	public function getAuth_level(){
		return $this->sessionSet('auth_level');
	}

	public function setAuth_level($auth_level){
		$_SESSION['auth_level'] = (int) $auth_level;
	}

	public function getContact_name(){
		return $this->sessionSet('contact_name');
	}

	public function setContact_name($contact_name){
		$_SESSION['contact_name'] = $contact_name;
	}

	public function setZip($zip) {
		$_SESSION['zip'] = $zip;
	}

	public function getZip() {
		//return $this->sessionSet($_SESSION['zip']);
		return $this->sessionSet('zip');
	}

	public function getOrder_id() {
		return $this->sessionSet($_SESSION['order_id']);
	}

	public function setOrder_id($order_id) {
		$_SESSION['order_id'] = $order_id;
	}

	public function setBillAddress_id($address_id) {
		$_SESSION['bill_address_id'] = (int) $address_id;
	}

	public function getBillAddress_id() { // currently selected billing address when logged in
		return $this->sessionSet('bill_address_id');
	}

	public function setShipAddress_id($address_id) {
		$_SESSION['ship_address_id'] = (int) $address_id;
	}

	public function getShipAddress_id() { // currently selected shipping address when logged in
		return $this->sessionSet('ship_address_id');
	}

}