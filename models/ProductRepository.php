<?php

namespace models;
use PDO;

class ProductRepository
{
    
    /**
     * @var PDO
     */
    private $Pdo;
    
    public function __construct(PDO $Pdo)
    {
        $this->Pdo = $Pdo;
    }
        
    public function loadProduct($product_id,$withimages=0)
    {
        // step 1:  load main product attributes into product object

        $Stm = $this->Pdo->prepare('
            SELECT p.product_id,p.model,p.price,p.prodinfo,p.is_purchasable,pi.image_path
            FROM products p LEFT JOIN product_images pi
            ON p.product_id = pi.product_id
            WHERE p.product_id = :product_id
            AND pi.is_primary = 1
        ');
        $Stm->bindParam(':product_id',$product_id,PDO::PARAM_INT);
        $Stm->execute();

        $productrow = $Stm->fetch(PDO::FETCH_ASSOC);
        //var_dump($productrow);

        if ($productrow) { 
        return $this->arrayToObject($productrow);
        }
        else return null;

        // step 2: load array of images into product object image_array
    }

    public function listProducts($category_id)
    {
        $Stm = $this->Pdo->prepare('
            SELECT p.product_id,p.model,p.url_slug,p.price,p.prodinfo,pi.image_path
            FROM products p LEFT JOIN product_images pi
            ON p.product_id = pi.product_id
            WHERE p.category = :category_id
            AND pi.is_primary = 1
            ORDER by p.product_id ASC
        ');
        $Stm->bindParam(':category_id',$category_id,PDO::PARAM_STR);
        $Stm->execute();
        
        $resultset = $Stm->fetchall(PDO::FETCH_ASSOC);

        $productlist = array();

        foreach ($resultset as $productrow) {

            $Currentproduct = $this->arrayToObject($productrow);
            $productlist[] = $Currentproduct;

        }

        return $productlist;

    }

    
    private function arrayToObject(array $array)
    {
        $Product = new Product();
        $Product->setId($array['product_id']);
        $Product->setModel($array['model']);
        $Product->setPrice($array['price']);
        $Product->setProdInfo($array['prodinfo']);
        $Product->setPrimaryImagePath($array['image_path']);
        if (isset($array['is_purchasable'])) {
            $Product->setIsPurchasable($array['is_purchasable']);
        }
        if (isset($array['url_slug'])) {
        $Product->setUrl_slug($array['url_slug']);
        }
        return $Product;
    }
}

class ProductWithImagesRepository extends ProductRepository
{

    public function loadProductImages($product_id)
    {
        $Stm = $this->Pdo->prepare('
            SELECT pi.image_path,pi.is_primary
            FROM products p LEFT JOIN product_images pi
            ON p.product_id = pi.product_id
            WHERE p.product_id = :product_id 
            AND pi.is_primary = 1
        ');
        $Stm->bindParam(':product_id',$product_id,PDO::PARAM_INT);
        $Stm->execute();
        return $this->arrayToObject($Stm->fetchAssoc());
    }
}

/*
SELECT p.product_id,p.model,p.price,p.prodinfo,p.rank, pi.image_path
    FROM products p LEFT JOIN product_images pi
    ON p.product_id = pi.product_id
    WHERE p.category=? 
    AND pi.is_primary=1
    ORDER BY p.rank ASC
*/

?>



