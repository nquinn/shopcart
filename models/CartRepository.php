<?php

namespace models;
use PDO;

class CartRepository {

	private $Pdo;
    
    public function __construct(PDO $Pdo)
    {
        $this->Pdo = $Pdo;
    }

    public function isProductInCart($product_id,$session_id) {

    	$Stm = $this->Pdo->prepare('
			SELECT product_id from temp_orders
			WHERE product_id = :product_id
			AND sesid = :sesid
			');

		$Stm->bindParam(':product_id',$product_id,PDO::PARAM_INT);
		$Stm->bindParam(':sesid',$session_id,PDO::PARAM_STR);
		$Stm->execute();

		$rowa = $Stm->fetch(PDO::FETCH_ASSOC);
	
		if ($rowa['product_id'] == $product_id) { 
			return 1;
		}
		else return 0;
    }

	public function addProduct($product_id,$qty,$session_id){

		$temptime = date("Y-m-d");

		// check to see if item is already in the cart

		
		$Stm = $this->Pdo->prepare('
			SELECT product_id from temp_orders
			WHERE product_id = :product_id
			AND sesid = :sesid
			');

		$Stm->bindParam(':product_id',$product_id,PDO::PARAM_INT);
		$Stm->bindParam(':sesid',$session_id,PDO::PARAM_STR);
		$Stm->execute();

		$rowa = $Stm->fetch(PDO::FETCH_ASSOC);
	
		if ($rowa['product_id'] == $product_id) { 
			$_SESSION['cart_alert'] = 'Item already exists in cart'; 
		}

		if ($rowa['product_id'] != $product_id) {

		// get database price to store in temp_orders
			
		$Stm = $this->Pdo->prepare('
			SELECT price from products
			WHERE product_id = :product_id
			');

		$Stm->bindParam(':product_id',$product_id,PDO::PARAM_INT);
		$Stm->execute();

		$rowb = $Stm->fetch(PDO::FETCH_ASSOC);
		$currentprice = $rowb['price'];
		
		// now store everything (could retrieve this all as an object instead)

		$Stm = $this->Pdo->prepare('
            INSERT into temp_orders 
            (sesid,product_id,price,qty,temptime)
            VALUES 
            (:sesid,:product_id,:price,:qty,:temptime)
        ');
        $Stm->bindParam(':sesid',$session_id,PDO::PARAM_STR);
        $Stm->bindParam(':product_id',$product_id,PDO::PARAM_INT);
        $Stm->bindParam(':price',$currentprice,PDO::PARAM_INT);
        $Stm->bindParam(':qty',$qty,PDO::PARAM_INT);
        $Stm->bindParam(':temptime',$temptime,PDO::PARAM_STR);
        $Stm->execute();

       } // end !isset(rowa) - product does not exist in cart yet so it's ok to add
	}

	public function deleteProduct($product_id,$session_id){

		$Stm = $this->Pdo->prepare('
            DELETE FROM temp_orders             
            WHERE sesid = :sesid
            AND product_id = :product_id
        ');
        $Stm->bindParam(':sesid',$session_id,PDO::PARAM_STR);
        $Stm->bindParam(':product_id',$product_id,PDO::PARAM_INT);
        $Stm->execute();
	}

	public function changeQty($product_id,$qty,$session_id){

		$Stm = $this->Pdo->prepare('
            UPDATE temp_orders 
            SET qty = :qty 
            WHERE sesid = :sesid
            AND product_id = :product_id
        ');
        $Stm->bindParam(':qty',$qty,PDO::PARAM_INT);
        $Stm->bindParam(':sesid',$session_id,PDO::PARAM_STR);
        $Stm->bindParam(':product_id',$product_id,PDO::PARAM_INT);
        $Stm->execute();

	}


	public function loadLineItems(){

		$session_id = session_id();

		$cartRowArray = array();

		$Stm = $this->Pdo->prepare('
            SELECT t.product_id, t.price, t.qty, p.model, p.weight
            FROM temp_orders t LEFT JOIN products p          
            ON t.product_id = p.product_id
            WHERE sesid = :sesid
        ');
        $Stm->bindParam(':sesid',$session_id,PDO::PARAM_STR);
        $Stm->execute();

        $resultset = $Stm->fetchall(PDO::FETCH_ASSOC);
		
		$subtotal = 0;

		foreach ($resultset as $productrow) {
			
			$currentid = $productrow['product_id'];
			$currentprice = $productrow['price'];
			$currentqty = $productrow['qty'];
			$currentmodel = $productrow['model'];
			$currentweight = $productrow['weight'];
			$linetotal = $currentprice*$currentqty;

			$subtotal = $subtotal + $linetotal;

		$CartRow = new CartRow();	
		$CartRow->setProduct_Id($currentid);
		$CartRow->setModel($currentmodel);
		$CartRow->setPrice($currentprice);
		$CartRow->setQty($currentqty);
		$CartRow->setLine_Total($linetotal);
		$CartRow->setWeight($currentweight);

		$cartRowArray[] = $CartRow;

		}
		return $cartRowArray;
	}

	public function arrayToObject(array $array){
		$CartRow = new CartRow();
		// let's just do the heavy lifting above instead
		// since there are 2 queries

	}

	public function loadSubtotal() {
		$session_id = session_id();
		
		$Stm = $this->Pdo->prepare('
            SELECT price, qty
            FROM temp_orders
            WHERE sesid = :sesid
        ');
        $Stm->bindParam(':sesid',$session_id,PDO::PARAM_STR);
        $Stm->execute();

        $resultset = $Stm->fetchall(PDO::FETCH_ASSOC);
		
		$subtotal = 0;

		foreach ($resultset as $productrow) {
			
			$currentprice = $productrow['price'];
			$currentqty = $productrow['qty'];
			$linetotal = $currentprice*$currentqty;

			$subtotal = $subtotal + $linetotal;

		}
		return $subtotal;
	}

	private function checkZip($zip) {

		if (strlen($zip) > 4) {

			$zip = sprintf("%05d",$zip); // need it with leading zeros so we can look at first 3
			$shortzip = substr($zip,0,3); // table only uses first 3

			$Stme = $this->Pdo->prepare('
			SELECT destination_zip,ending_zip,shipping_zone
			FROM shipping_zones
			WHERE destination_zip <= :shortzip
			ORDER BY destination_zip DESC
			LIMIT 1');

			$Stme->bindParam(':shortzip',$shortzip,PDO::PARAM_STR);
			$Stme->execute();

			if ($Stme->fetch(PDO::FETCH_ASSOC)) {
				return 1;
			}

		} // end zip is at least 5 digits
		else return null;

	}

	public function loadShipping(){

		$User = new \models\User();
		$zip = $User->getZip();
		
		if ($this->checkZip($zip)) {

			$box_weights = $this->shippingLoadBoxWeights();
			$rawshipping = $this->shippingLoadMultipleRates($box_weights,$zip); // combine with fuel and zone surcharge
			$special_discount = $this->shippingSpecialDiscount(); // subtract certain amounts for low cost orders
			$handling = $this->shippingLoadHandling($box_weights); // charge $3 per box
			$shipping = $rawshipping - $special_discount + $handling;
			return $shipping;
		}
		else {
			return null;
		}

	}

	public function loadTax(){

		$zip = $_SESSION['zip'];

		// check to see if zip is in illinois first

		$checktax = $this->Pdo->prepare('
			SELECT zipcode 
			FROM illinois_zip_codes 
			WHERE zipcode = :zip
			');

		$checktax->bindParam(':zip',$zip,PDO::PARAM_STR); // not sure if I can use the leading 0 zip here
		$checktax->execute();

		if ($checktax->fetch(PDO::FETCH_ASSOC)) {
			$subtotal = $this->loadSubtotal();
			$tax = $subtotal * .095;
			return $tax;
		}
		else return 0;
	}

	public function loadTotal() {
		$total = $this->loadSubtotal() + $this->loadTax() + $this->loadShipping();
		return $total;
	}

	public function saveOrder($customerarray,$CartInfo) {

		// NPQTODO:  verify that customer email address doesn't already exist

		$customer_id = $this->saveCustomer($customerarray);
		$ship_address_id = $this->saveCustomerShipAddress($customer_id,$customerarray);
		$bill_address_id = $this->saveCustomerBillAddress($customer_id,$customerarray);
		$order_id = $this->saveFinalOrder($customer_id,$ship_address_id,$bill_address_id,$CartInfo);
		$this->saveOrderDetails($order_id);
		return $order_id;
		
	}

	public function saveOrderLoggedIn($CartInfo) {

		$User = new \models\User();
		$customer_id = $User->getCustomer_id();
		$ship_address_id = $User->getShipAddress_id();
		$bill_address_id = $User->getBillAddress_id();
		$order_id = $this->saveFinalOrder($customer_id,$ship_address_id,$bill_address_id,$CartInfo);
		$this->saveOrderDetails($order_id);
		return $order_id;
		
	}

	public function saveCustomer($customerarray) {


		$tempdate = date("Y-m-d");

		$Stm = $this->Pdo->prepare('
            INSERT into customers
            (contact_name, contact_email, contact_phone, create_date, password_hash, auth_level)
			VALUES
			(:contact_name, :contact_email, :contact_phone, :create_date, :password_hash, :auth_level)
        ');

        $Stm->bindParam(':contact_name',$customerarray['contact_name'],PDO::PARAM_STR);
		$Stm->bindParam(':contact_email',$customerarray['contact_email'],PDO::PARAM_STR);
		$Stm->bindParam(':contact_phone',$customerarray['contact_phone'],PDO::PARAM_STR);
		$Stm->bindParam(':create_date',$tempdate,PDO::PARAM_STR);

		$password_hash = password_hash($customerarray['password'],PASSWORD_DEFAULT);
		$Stm->bindParam(':password_hash',$password_hash,PDO::PARAM_STR);

		$auth_level = 1; // default user auth level
		$Stm->bindParam(':auth_level',$auth_level,PDO::PARAM_INT);

        $Stm->execute();

        $customer_id = $this->Pdo->lastInsertId();

		return $customer_id;
	}

	public function saveCustomerShipAddress($customer_id,$customerarray) {

		$Stm = $this->Pdo->prepare('
            INSERT into customers_addresses
            (customer_id, first, last, company, address1, address2, city, state, zip, address_type)
			VALUES
			(:customer_id,:first,:last,:company,:address1,:address2,:city,:state,:zip,:address_type)
        ');

		$address_type = 's'; // s = shipping, b = billing

        $Stm->bindParam(':customer_id',$customer_id,PDO::PARAM_INT);
		$Stm->bindParam(':first',$customerarray['s_first'],PDO::PARAM_STR);
		$Stm->bindParam(':last',$customerarray['s_last'],PDO::PARAM_STR);
		$Stm->bindParam(':company',$customerarray['s_company'],PDO::PARAM_STR);
		$Stm->bindParam(':address1',$customerarray['s_address1'],PDO::PARAM_STR);
		$Stm->bindParam(':address2',$customerarray['s_address2'],PDO::PARAM_STR);
		$Stm->bindParam(':city',$customerarray['s_city'],PDO::PARAM_STR);
		$Stm->bindParam(':state',$customerarray['s_state'],PDO::PARAM_STR);
		$Stm->bindParam(':zip',$customerarray['s_zip'],PDO::PARAM_STR);
		$Stm->bindParam(':address_type',$address_type,PDO::PARAM_STR);

        $Stm->execute();

        $ship_address_id = $this->Pdo->lastInsertId();
        return $ship_address_id;

	}

	public function saveCustomerBillAddress($customer_id,$customerarray) {

		$Stm = $this->Pdo->prepare('
            INSERT into customers_addresses
            (customer_id, first, last, company, address1, address2, city, state, zip, address_type)
			VALUES
			(:customer_id,:first,:last,:company,:address1,:address2,:city,:state,:zip,:address_type)
        ');

        $address_type = 'b'; // s = shipping, b = billing

        $Stm->bindParam(':customer_id',$customer_id,PDO::PARAM_INT);
		$Stm->bindParam(':first',$customerarray['b_first'],PDO::PARAM_STR);
		$Stm->bindParam(':last',$customerarray['b_last'],PDO::PARAM_STR);
		$Stm->bindParam(':company',$customerarray['b_company'],PDO::PARAM_STR);
		$Stm->bindParam(':address1',$customerarray['b_address1'],PDO::PARAM_STR);
		$Stm->bindParam(':address2',$customerarray['b_address2'],PDO::PARAM_STR);
		$Stm->bindParam(':city',$customerarray['b_city'],PDO::PARAM_STR);
		$Stm->bindParam(':state',$customerarray['b_state'],PDO::PARAM_STR);
		$Stm->bindParam(':zip',$customerarray['b_zip'],PDO::PARAM_STR);
		$Stm->bindParam(':address_type',$address_type,PDO::PARAM_STR);

        $Stm->execute();

        $bill_address_id = $this->Pdo->lastInsertId();
        return $bill_address_id;
		
	}

	public function saveFinalOrder($customer_id,$ship_address_id,$bill_address_id,$CartInfo) {

		$session_id = session_id();
		$tempdate = date("Y-m-d");

		$track = $CartInfo->getTrack();
        $refer = $CartInfo->getRefer();
        $campaign = $CartInfo->getCampaign();
        $keyword = $CartInfo->getKeyword();

        $subtotal = $this->loadSubtotal();
        $tax = $this->loadTax();
        $shipping = $this->loadShipping();
        $total = $this->loadTotal();

       
        $Stmb = $this->Pdo->prepare('
        INSERT into final_order
        (customer_id,ship_address_id,bill_address_id,order_date,track,refer,campaign,keyword,subtotal,tax,shipping,total)
        VALUES
        (:customer_id,:ship_address_id,:bill_address_id,:order_date,:track,:refer,:campaign,:keyword,:subtotal,:tax,:shipping,:total)
        ');

		$Stmb->bindParam(':customer_id',$customer_id,PDO::PARAM_INT);
		$Stmb->bindParam(':ship_address_id',$ship_address_id,PDO::PARAM_INT);
		$Stmb->bindParam(':bill_address_id',$bill_address_id,PDO::PARAM_INT);
		$Stmb->bindParam(':order_date',$tempdate,PDO::PARAM_STR);
		$Stmb->bindParam(':track',$track,PDO::PARAM_STR);
		$Stmb->bindParam(':refer',$refer,PDO::PARAM_STR);
		$Stmb->bindParam(':campaign',$campaign,PDO::PARAM_STR);
		$Stmb->bindParam(':keyword',$keyword,PDO::PARAM_STR);
		$Stmb->bindParam(':subtotal',$subtotal,PDO::PARAM_INT);
		$Stmb->bindParam(':tax',$tax,PDO::PARAM_INT);
		$Stmb->bindParam(':shipping',$shipping,PDO::PARAM_INT);
		$Stmb->bindParam(':total',$total,PDO::PARAM_INT);

		$Stmb->execute();

		$order_id = $this->Pdo->lastInsertId();

		return $order_id;

	}


	public function saveOrderDetails($order_id) {

		$Cartrowarray = $this->loadLineItems();

		foreach ($Cartrowarray as $Product){

		$product_id = $Product->getProduct_id();
		$qty = $Product->getQty();
		$price = $Product->getPrice();
		$discount = $Product->getDiscount();


		$Stmc = $this->Pdo->prepare('
        INSERT into order_detail
        (product_id,qty,price,order_id,discount)
        VALUES
        (:product_id,:qty,:price,:order_id,:discount)
        ');

		$Stmc->bindParam(':product_id',$product_id,PDO::PARAM_INT);
		$Stmc->bindParam(':qty',$qty,PDO::PARAM_INT);
		$Stmc->bindParam(':price',$price,PDO::PARAM_INT);
		$Stmc->bindParam(':order_id',$order_id,PDO::PARAM_INT);
		$Stmc->bindParam(':discount',$discount,PDO::PARAM_STR);
		

		$Stmc->execute();

		}	
	}

	public function shippingLoadBoxWeights() {

		// build a collection of shipping boxes and fill them with products.
		// products inserted based on weight descending, with a max of 45lbs per box
		// returns an array of just the individual box rates

		$session_id = session_id();

		$cartrowarray = $this->loadLineItems();

		$Stmd = $this->Pdo->prepare('
		SELECT t.product_id,t.qty,p.weight
		FROM products p, temp_orders t
		WHERE p.product_id = t.product_id
		AND t.sesid = :sesid
		ORDER BY p.weight DESC
		');

		$Stmd->bindParam(':sesid',$session_id,PDO::PARAM_STR);
		$Stmd->execute();
		$resultset = $Stmd->fetchall(PDO::FETCH_ASSOC);
		
		$totalweight = 0;

		foreach ($resultset as $row) {

		$currentpid = $row['product_id'];
		$currentqty = $row['qty'];
		$currentweightperitem = $row['weight'];
		$currentweight = $currentqty * $currentweightperitem;
		$totalweight = $totalweight + $currentweight;

			// for each item, we have to loop through the qty's to add each item separately
			for ($k=0; $k<$currentqty; $k++) {
			$all_item_weights[] = $currentweightperitem;
			}
		
		}	
		
		$num_items = count($all_item_weights);

		$runningweight = 0; // increment this until it hits 45, then reset and build new box

		for($count=0; $count<$num_items; $count++) {

		$runningweight = $runningweight + $all_item_weights[$count]; // add one item at a time to a box

		if(isset($all_item_weights[$count+1])) {
			$nextweight = $all_item_weights[$count+1];
		}
		else $nextweight = 0;

		if ((($runningweight + $nextweight) > 46) || $count == $num_items-1) { // create the box in array if we are about to go over, or if we are on last object
		$box_weights[] = $runningweight;
		$runningweight = 0; // reset runningweight for next box
		}

		}

		return $box_weights;
	}

	public function shippingLoadRates($weight,$zip) {

		// for each box, get the raw shipping rate from our UPS/USPS lookup table
		// also, let's include the fuel surcharge and zone surcharge here rather than
		// separating them out

		$zip = sprintf("%05d",$zip); // need it with leading zeros so we can look at first 3
		$shortzip = substr($zip,0,3); // table only uses first 3

		$Stme = $this->Pdo->prepare('
		SELECT destination_zip,ending_zip,shipping_zone
		FROM shipping_zones
		WHERE destination_zip <= :shortzip
		ORDER BY destination_zip DESC
		LIMIT 1');

		$Stme->bindParam(':shortzip',$shortzip,PDO::PARAM_STR);
		$Stme->execute();
		$resultset = $Stme->fetch(PDO::FETCH_ASSOC);

		$shipping_zone = $resultset['shipping_zone'];
		$ending_zip = $resultset['ending_zip'];

		if ($shortzip > $ending_zip) { 
		  echo"<p>Error: no such zip code</p>"; 
		  $_SESSION['shipzone'] = "999999999"; // not sure why I did this, some kind of error alert
		  }

		else { $_SESSION['shipzone'] = $shipping_zone; }  

		// ok, we have the shipping zone, now let's lookup the UPS rate with that + weight
		// this all assumes 1 box for now.  will fix that later.  Also, remember that we need to adjust rates up to
		// 6 pounds since we use USPS instead!

		$zone_col_name = "zone".$shipping_zone; // name of column in db
		$weight = ceil($weight); // need to round up to next integer

		$Stmf = $this->Pdo->prepare("
		SELECT $zone_col_name AS rawshipcost
		FROM ups_rates
		WHERE weight = :weight
		");
		$Stmf->bindParam(':weight',$weight,PDO::PARAM_INT);
		$Stmf->execute();
		$resultsetb = $Stmf->fetch(PDO::FETCH_ASSOC);

		$rawshipcost = $resultsetb['rawshipcost']*100;

		// still need to look for zone surcharge

		$Stmg = $this->Pdo->prepare('
		SELECT zipcode 
		FROM ups_area_surcharge
		WHERE zipcode = :zip
		');
		$Stmg->bindParam(':zip',$zip,PDO::PARAM_STR); // not sure if I can use the leading 0 zip here
		$Stmg->execute();
		
		$num_rows = $Stmg->fetch(PDO::FETCH_ASSOC);

		// NPQ to do - this could be wrong

		if ($num_rows && $weight > 5) {
		$shipping_with_zone = $rawshipcost + 300; // add $3 (in cents) zone surcharge if through UPS and in surcharge zone
		}
		else ($shipping_with_zone = $rawshipcost);

		// optional:  add surcharge for residential - $2.80
		// just always add it?  check for it?  split the difference?
		
		// then add fuel surcharge, only for UPS orders
		if ($weight > 5) {
		$shipping_rate = $shipping_with_zone * 1.08; // 8% fuel surcharge (depends on fuel prices, ranges from 7.5-8.5% usually)
		}
		else $shipping_rate = $shipping_with_zone;

		return $shipping_rate; // convert to cents
	}

	public function shippingLoadMultipleRates($box_weights,$zip) {
		
		$shipping_cost = 0;
		foreach ($box_weights as $box) {
			$shipping_cost = $shipping_cost + $this->shippingLoadRates($box,$zip); // this is probably wrong
		}
		return $shipping_cost;
	}

	public function shippingSpecialDiscount() {
		
		// any special adjustments needed like discounts for a single 4oz can, etc
		// this is very hacky and for custom adjustments

		$cartrowarray = $this->loadLineItems();
		$numuniques = count($cartrowarray);

		if ($numuniques ==1) {

			$ProdObject = $cartrowarray[0];

			if(
				($ProdObject->getProduct_id() == 1 && $ProdObject->getQty() == 1))
			 { return 100; } // if a single 12oz can order only, discount $1 (100 cents)
			elseif(
				($ProdObject->getProduct_id() == 3 && $ProdObject->getQty() == 1))
			 { return 200; } // if a single 4oz can order only, discount $2 (200 cents)
			else return 0;


		}
		
	}

	public function shippingLoadHandling($box_weights) {

		// $3 for first box, $2 for each additional box
		
		$numfinalboxes = count($box_weights);
		$handling = 300 + ($numfinalboxes-1)*200; // in cents
		return $handling;
	}

}