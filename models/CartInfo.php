<?php

namespace models;

class CartInfo {
	
	private $refer;
	private $campaign;
	private $keyword;
	private $track;

	 public function __construct()
    {
        $_SESSION['cartinfo_set'] = 1; // set this on first creation so we can check to see if it's set later
    }

	public function getRefer(){
	return $this->refer;
	}

	public function setRefer(){
		$this->refer = $_SESSION['refer'];
	}

	public function getCampaign(){
		return $this->campaign;
	}

	public function setCampaign(){
		$this->campaign = $_SESSION['campaign'];
	}

	public function getKeyword(){
		return $this->keyword;
	}

	public function setKeyword(){
		$this->keyword = $_SESSION['keyword'];
	}

	public function getTrack(){
		return $this->track;
	}

	public function setTrack(){
		$this->track = $_SESSION['track'];
	}


}