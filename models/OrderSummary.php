<?php

namespace models;

class OrderSummary {
	
	private $order_id;
	private $customer_id;
	private $order_date;
	private $track;
	private $trans_id;
	private $auth_code;
	private $response_code;
	private $subtotal;
	private $tax;
	private $shipping;
	private $total;
	private $refer;
	private $campaign;
	private $keyword;

	private $shipping_address; // this will be an object
	private $billing_address; // this will be an object


	public function getOrder_id(){
		return $this->order_id;
	}

	public function setOrder_id($order_id){
		$this->order_id = $order_id;
	}

	public function getCustomer_id(){
		return $this->customer_id;
	}

	public function setCustomer_id($customer_id){
		$this->customer_id = $customer_id;
	}

	public function getOrder_date(){
		return $this->order_date;
	}

	public function setOrder_date($order_date){
		$this->order_date = $order_date;
	}

	public function setRefer($refer) {
		$this->refer = $refer;
	}

	public function getRefer() {
		return $this->refer;
	}

	public function setCampaign($campaign) {
		$this->campaign = $campaign;
	}

	public function getCampaign() {
		return $this->campaign;
	}

	public function setKeyword($keyword) {
		$this->keyword = $keyword;
	}

	public function getKeyword() {
		return $this->keyword;
	}


	public function getTrack(){
		return $this->track;
	}

	public function setTrack($track){
		$this->track = $track;
	}

	public function getTrans_id(){
		return $this->trans_id;
	}

	public function setTrans_id($trans_id){
		$this->trans_id = $trans_id;
	}

	public function getAuth_code(){
		return $this->auth_code;
	}

	public function setAuth_code($auth_code){
		$this->auth_code = $auth_code;
	}

	public function getResponse_code(){
		return $this->response_code;
	}

	public function setResponse_code($response_code){
		$this->response_code = $response_code;
	}

	public function getSubtotal(){
		return $this->subtotal;
	}

	public function setSubtotal($subtotal){
		$this->subtotal = $subtotal;
	}

	public function getTax(){
		return $this->tax;
	}

	public function setTax($tax){
		$this->tax = $tax;
	}

	public function getShipping(){
		return $this->shipping;
	}

	public function setShipping($shipping){
		$this->shipping = $shipping;
	}

	public function getTotal(){
		return $this->total;
	}

	public function setTotal($total){
		$this->total = $total;
	}

	public function getContact_name(){
		return $this->contact_name;
	}

	public function setContact_name($contact_name){
		$this->contact_name = $contact_name;
	}

	public function getContact_email(){
		return $this->contact_email;
	}

	public function setContact_email($contact_email){
		$this->contact_email = $contact_email;
	}

	public function getContact_phone(){
		return $this->contact_phone;
	}

	public function setContact_phone($contact_phone){
		$this->contact_phone = $contact_phone;
	}

	public function getShipping_address(){
		return $this->shipping_address;
	}

	public function setShipping_address($shipping_address){
		$this->shipping_address = $shipping_address;
	}

	public function getBilling_address(){
		return $this->billing_address;
	}

	public function setBilling_address($billing_address){
		$this->billing_address = $billing_address;
	}


}