<?php

namespace models;

class Article {
	
	private $article_id;
	private $article_date;
	private $title;
	private $content;
	private $category;

	public function getArticle_id(){
		return $this->article_id;
	}

	public function setArticle_id($article_id){
		$this->article_id = $article_id;
	}

	public function getArticle_date(){
		return $this->article_date;
	}

	public function setArticle_date($article_date){
		$this->article_date = $article_date;
	}

	public function getTitle(){
		return $this->title;
	}

	public function setTitle($title){
		$this->title = $title;
	}

	public function getContent(){
		return $this->content;
	}

	public function setContent($content){
		$this->content = $content;
	}

	public function getCategory(){
		return $this->category;
	}

	public function setCategory($category){
		$this->category = $category;
	}

}