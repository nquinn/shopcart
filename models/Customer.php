<?php

namespace models;

class Customer {

	private $s_first_name;
	private $s_last_name;
	private $s_company_name;
	private $s_address1;
	private $s_address2;
	private $s_city;
	private $s_state;
	private $s_zip;

	private $b_first_name;
	private $b_last_name;
	private $b_company_name;
	private $b_address1;
	private $b_address2;
	private $b_city;
	private $b_state;
	private $b_zip;

	private $contact_email;
	private $phone;


	public function getS_first_name(){
		return $this->s_first_name;
	}

	public function setS_first_name($s_first_name){
		$this->s_first_name = $s_first_name;
	}

	public function getS_last_name(){
		return $this->s_last_name;
	}

	public function setS_last_name($s_last_name){
		$this->s_last_name = $s_last_name;
	}

	public function getS_company_name(){
		return $this->s_company_name;
	}

	public function setS_company_name($s_company_name){
		$this->s_company_name = $s_company_name;
	}

	public function getS_address1(){
		return $this->s_address1;
	}

	public function setS_address1($s_address1){
		$this->s_address1 = $s_address1;
	}

	public function getS_address2(){
		return $this->s_address2;
	}

	public function setS_address2($s_address2){
		$this->s_address2 = $s_address2;
	}

	public function getS_city(){
		return $this->s_city;
	}

	public function setS_city($s_city){
		$this->s_city = $s_city;
	}

	public function getS_state(){
		return $this->s_state;
	}

	public function setS_state($s_state){
		$this->s_state = $s_state;
	}

	public function getS_zip(){
		return $this->s_zip;
	}

	public function setS_zip($s_zip){
		$this->s_zip = $s_zip;
	}

	public function getB_first_name(){
		return $this->b_first_name;
	}

	public function setB_first_name($b_first_name){
		$this->b_first_name = $b_first_name;
	}

	public function getB_last_name(){
		return $this->b_last_name;
	}

	public function setB_last_name($b_last_name){
		$this->b_last_name = $b_last_name;
	}

	public function getB_company_name(){
		return $this->b_company_name;
	}

	public function setB_company_name($b_company_name){
		$this->b_company_name = $b_company_name;
	}

	public function getB_address1(){
		return $this->b_address1;
	}

	public function setB_address1($b_address1){
		$this->b_address1 = $b_address1;
	}

	public function getB_address2(){
		return $this->b_address2;
	}

	public function setB_address2($b_address2){
		$this->b_address2 = $b_address2;
	}

	public function getB_city(){
		return $this->b_city;
	}

	public function setB_city($b_city){
		$this->b_city = $b_city;
	}

	public function getB_state(){
		return $this->b_state;
	}

	public function setB_state($b_state){
		$this->b_state = $b_state;
	}

	public function getB_zip(){
		return $this->b_zip;
	}

	public function setB_zip($b_zip){
		$this->b_zip = $b_zip;
	}

	public function getContact_email(){
		return $this->contact_email;
	}

	public function setContact_email($contact_email){
		$this->contact_email = $contact_email;
	}

	public function getPhone(){
		return $this->phone;
	}

	public function setPhone($phone){
		$this->phone = $phone;
	}

}