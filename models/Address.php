<?php

namespace models;

class Address {
	
	private $first;
	private $last;
	private $company;
	private $address1;
	private $address2;
	private $city;
	private $state;
	private $zip;
	private $address_id;

	public function getFirst(){
		return $this->first;
	}

	public function setFirst($first){
		$this->first = $first;
	}

	public function getLast(){
		return $this->last;
	}

	public function setLast($last){
		$this->last = $last;
	}

	public function getCompany(){
		return $this->company;
	}

	public function setCompany($company){
		$this->company = $company;
	}

	public function getAddress1(){
		return $this->address1;
	}

	public function setAddress1($address1){
		$this->address1 = $address1;
	}

	public function getAddress2(){
		return $this->address2;
	}

	public function setAddress2($address2){
		$this->address2 = $address2;
	}

	public function getCity(){
		return $this->city;
	}

	public function setCity($city){
		$this->city = $city;
	}

	public function getState(){
		return $this->state;
	}

	public function setState($state){
		$this->state = $state;
	}

	public function getZip(){
		return $this->zip;
	}

	public function setZip($zip){
		$this->zip = $zip;
	}

	public function getAddress_id(){
		return $this->address_id;
	}

	public function setAddress_id($address_id){
		$this->address_id = $address_id;
	}

}