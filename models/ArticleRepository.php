<?php

namespace models;
use PDO;

class ArticleRepository {

    /**
     * @var PDO
     */
    private $Pdo;
    
public function __construct(PDO $Pdo)
    {
        $this->Pdo = $Pdo;
    }
	
	public function addArticle($title, $content, $category) {

		$Stm = $this->Pdo->prepare('
            INSERT into articles (article_date, title, content, category)
            VALUES (?, ?, ?, ?)
        ');
        $Stm->bindParam(1,$article_date,PDO::PARAM_STR);
        $Stm->bindParam(2,$title,PDO::PARAM_STR);
        $Stm->bindParam(3,$content,PDO::PARAM_STR);
        $Stm->bindParam(4,$category,PDO::PARAM_STR);
        $Stm->execute();

	}

	public function loadArticle($article_id) {

		$Stm = $this->Pdo->prepare('
            SELECT article_id, article_date, title, content, category FROM articles 
            WHERE article_id = ?
        ');
        $Stm->bindParam(1,$article_id,PDO::PARAM_INT);
        $Stm->execute();
        return $this->arrayToObject($Stm->fetch(PDO::FETCH_ASSOC));

	}

	private function arrayToObject(array $array)
    {
        $Article = new Article();
        $Article->setArticle_id($array['article_id']);
        $Article->setArticle_date($array['article_date']);
        $Article->setTitle($array['title']);
        $Article->setContent($array['content']);
        $Article->setCategory($array['category']);
        return $Article;
    }

	public function updateArticle($article_id, $article_date, $title, $content, $category) {

		$Stm = $this->Pdo->prepare('
            UPDATE articles 
            SET article_date = ?,
            title = ?,
            content = ?,
            category = ?
            WHERE article_id = ?
        ');
        $Stm->bindParam(1,$article_date,PDO::PARAM_STR);
        $Stm->bindParam(2,$title,PDO::PARAM_STR);
        $Stm->bindParam(3,$content,PDO::PARAM_STR);
        $Stm->bindParam(4,$category,PDO::PARAM_STR);
        $Stm->bindParam(5,$article_id,PDO::PARAM_INT);
        $Stm->execute();
	}

    public function loadHome() {
        // not sure what data we need here yet
    }
}