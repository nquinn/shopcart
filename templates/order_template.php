<html>
<head>
<title>Order #<?= $OrderSummary->getOrder_id(); ?></title>
</head>
<body>

<H1>Order #<?= $OrderSummary->getOrder_id(); ?></H1>
<p><strong>Order Date: <?= $OrderSummary->getOrder_date(); ?></strong></p>

<p><strong>Shipping Info</strong></p>

<table>
<tr><td>s_first</td><td><?= $Shipping_address->getFirst(); ?></td></tr>
<tr><td>s_last</td><td><?= $Shipping_address->getLast(); ?></td></tr>
<tr><td>s_company</td><td><?= $Shipping_address->getCompany(); ?></td></tr>
<tr><td>s_address1</td><td><?= $Shipping_address->getAddress1(); ?></td></tr>
<tr><td>s_address2</td><td><?= $Shipping_address->getAddress2(); ?></td></tr>
<tr><td>s_city</td><td><?= $Shipping_address->getCity(); ?></td></tr>
<tr><td>s_state</td><td><?= $Shipping_address->getState(); ?></td></tr>
<tr><td>s_zip</td><td><?= $Shipping_address->getZip(); ?></td></tr>
</table>

<p><strong>Billing Info</strong></p>

<table>
<tr><td>b_first</td><td><?= $Billing_address->getFirst(); ?></td></tr>
<tr><td>b_last</td><td><?= $Billing_address->getLast(); ?></td></tr>
<tr><td>b_company</td><td><?= $Billing_address->getCompany(); ?></td></tr>
<tr><td>b_address1</td><td><?= $Billing_address->getAddress1(); ?></td></tr>
<tr><td>b_address2</td><td><?= $Billing_address->getAddress2(); ?></td></tr>
<tr><td>b_city</td><td><?= $Billing_address->getCity(); ?></td></tr>
<tr><td>b_state</td><td><?= $Billing_address->getState(); ?></td></tr>
<tr><td>b_zip</td><td><?= $Billing_address->getZip(); ?></td></tr>
<tr><td>contact_name</td><td><?= $OrderSummary->getContact_name(); ?></td></tr>
<tr><td>contact_email</td><td><?= $OrderSummary->getContact_email(); ?></td></tr>
<tr><td>contact_phone</td><td><?= $OrderSummary->getContact_phone(); ?></td></tr>
</table>

<p><strong>Order and Merchant Account Info</strong></p>

<table>
<tr><td>Tracking Code</td><td><?= $OrderSummary->getTrack(); ?></td>
	<td>Refer</td><td><?= $OrderSummary->getRefer(); ?></td>
</tr>
<tr><td>Campaign</td><td><?= $OrderSummary->getCampaign(); ?></td>
	<td>Keyword</td><td><?= $OrderSummary->getKeyword(); ?></td>
</tr>
<tr><td>Trans_id</td><td><?= $OrderSummary->getTrans_id(); ?></td>
	<td>Auth_code</td><td><?= $OrderSummary->getAuth_code(); ?></td>
</tr>

</table>

<!-- loop through products here -->

<table>
	<tr><td>Item</td><td>Unit Price</td><td>Quantity</td><td>Line Total</td></tr>

<?php
foreach ($OrderItems as $CartRow):  
?>

<tr><td><?= $CartRow->getModel();?></td>
	<td><?= $Format->formatCurrency($CartRow->getPrice());?></td>
	<td><?= $CartRow->getQty();?></td>
	<td><?= $Format->formatCurrency($CartRow->getLine_Total());?></td>
</tr>

<? endforeach; ?> 

<tr><td colspan=2></td><td>Subtotal</td><td><?= $Format->formatCurrency($OrderSummary->getSubtotal()); ?></td></tr>
<tr><td colspan=2></td><td>Tax</td><td><?= $Format->formatCurrency($OrderSummary->getTax()); ?></td></tr>
<tr><td colspan=2></td><td>Shipping</td><td><?= $Format->formatCurrency($OrderSummary->getShipping()); ?></td></tr>
<tr><td colspan=2></td><td>Total</td><td><?= $Format->formatCurrency($OrderSummary->getTotal()); ?></td></tr>
</table>



</body>
</html>


