<div class="mylogo">NoCorrosion</div>
<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li><a href="/">Home</a></li>
      <li class="divider"></li>
      
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="/category/commercial">Commercial</a></li>
          <li><a href="/category/industrial">Industrial</a></li>
          <li><a href="/category/greases">Greases</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
          <li class="divider"></li>
          <li><a href="#">One more separated link</a></li>
        </ul>
      </li>
      <li class="divider"></li>
      <li><a href="/faq">FAQ</a></li>
      <li class="divider"></li>
      <li><a href="/contact">Contact Us</a></li>
    </ul>
    
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/cart">Shopping Cart</a></li>
    </ul>   
  </div><!-- /.navbar-collapse -->
</nav>