<html>
<head>
	<script type="text/javascript">
    function autoClick()
    {
    	//document.getElementById("startrunning").click();
        //document.authorizeForm.submit();
    }
	</script>
</head>
<body onload="autoClick()">

<H1>Transferring you to Authorize.net for Payment &gt;&gt;&gt;</H1>

<form id="authorizeForm" method='post' action="<?= $auth_url ?>">
  
<input type='hidden' name="x_login" value="<?php echo $api_login_id?>" />
<input type='hidden' name="x_fp_hash" value="<?php echo $fingerprint?>" />
<input type='hidden' name="x_amount" value="<?= $Format->formatDollars($OrderSummary->getTotal()); ?>" />
<input type='hidden' name="x_fp_timestamp" value="<?php echo $fp_timestamp?>" />
<input type='hidden' name="x_fp_sequence" value="<?php echo $fp_sequence?>" />
<input type='hidden' name="x_version" value="3.1">
<input type='hidden' name="x_show_form" value="payment_form">
<input type='hidden' name="x_method" value="cc">
<INPUT TYPE='HIDDEN' NAME='x_type' VALUE='AUTH_CAPTURE'>
<input type='hidden' name='x_relay_response' value='TRUE'>
<input type='hidden' name='x_relay_url' value='<?= $response_url ?>' />
<input type='hidden' name='x_invoice_num' value='<?= $OrderSummary->getOrder_id(); ?>' />

<!-- billing info -->
<input type='hidden' name='x_first_name' value='<?= $Billing_address->getFirst(); ?>' />
<input type='hidden' name='x_last_name' value='<?= $Billing_address->getLast(); ?>' />
<input type='hidden' name='x_company' value='<?= $Billing_address->getCompany(); ?>' />
<input type='hidden' name='x_address' value='<? echo $Billing_address->getAddress1()." ".$Billing_address->getAddress2(); ?>' />
<input type='hidden' name='x_city' value='<?= $Billing_address->getCity(); ?>' />
<input type='hidden' name='x_state' value='<?= $Billing_address->getState(); ?>' />
<input type='hidden' name='x_zip' value='<?= $Billing_address->getCity(); ?>' />
<input type='hidden' name='x_phone' value='<?= $OrderSummary->getContact_phone(); ?>' />
<input type='hidden' name='x_email' value='<?= $OrderSummary->getContact_email(); ?>' />
<!-- send a double receipt to customer? -->
<!-- <input type='hidden' name='x_email_customer' value='TRUE' /> -->

<? 
$receiptheaderbody = "Thank you for your order from NoCorrosion.com!\n
You may visit this page to track your order:\n
http://www.nocorrosion.com/order/track/".$OrderSummary->getOrder_id()."\n
Please allow 1-2 business days for us to update the page.";
?>

<input type='hidden' name='x_header_email_receipt' value="<? echo"$receiptheaderbody";?>" />
<input type='hidden' name='x_cust_id' value='<?= $OrderSummary->getCustomer_id(); ?>' />

<!-- shipping info -->
<input type='hidden' name='x_ship_to_first_name' value='<?= $Shipping_address->getFirst(); ?>' />
<input type='hidden' name='x_ship_to_last_name' value='<?= $Shipping_address->getLast(); ?>' />
<input type='hidden' name='x_ship_to_company' value='<?= $Shipping_address->getCompany(); ?>' />
<input type='hidden' name='x_ship_to_address' value='<? echo $Shipping_address->getAddress1()." ".$Shipping_address->getAddress2(); ?>' />
<input type='hidden' name='x_ship_to_city' value='<?= $Shipping_address->getCity(); ?>' />
<input type='hidden' name='x_ship_to_state' value='<?= $Shipping_address->getState(); ?>' />
<input type='hidden' name='x_ship_to_zip' value='<?= $Shipping_address->getZip(); ?>' />

<!-- skipping tax and shipping here as optional extra fields -->

<?
$paymentheader = 'newheaderhere //NPQTODO';
?>

<!-- customize header and footer -->
<input type='hidden' name='x_header2_html_payment_form' value='<? echo"$paymentheader";?>' />
<input type='hidden' name='x_header2_html_receipt' value='<p>NoCorrosion Receipt Page</p><hr>' />
<!-- <input type='hidden' name='x_footer2_html_payment_form' value='<p>NoCorrosion Payment Footer Here</p>' />
<input type='hidden' name='x_footer2_html_receipt' value='<p>NoCorrosion Receipt Footer Here</p>' /> -->
<input type='hidden' name='x_color_background' value='white' />
<input type='hidden' name='x_color_text' value='black' />
<input type='hidden' name='x_cancel_url' value='https://www.nocorrosion.com/cart' />

<input type='hidden' name='x_description' value='NoCorrosion.com Order #<?= $OrderSummary->getOrder_id();?> Payment' />

<!-- <INPUT TYPE="HIDDEN" NAME="x_test_request" VALUE="TRUE"> -->
<? if(isset($test_request)) { echo $test_request; } ?>
<input id='startrunning' type='submit' value="Click here to make your payment">
</form>


</body>
</html>