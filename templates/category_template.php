<!DOCTYPE html>
<html>

	<head>
	    <title>Category List</title>
		<? include('head_links_template.php'); ?>
	</head>

	<body>

		<div class="container">
		<? include('nav_template.php'); ?>

			<?
			foreach ($productarray as $Product):
			?>

			<div class="row top10">
			    <div class="col-sm-3 centersmall">
			      <a href="/product/<?= $Product->getId()."/".$Product->getUrl_slug();?>">
			      <img width=150 src="<?= $Product->getPrimaryImagePath(); ?>">
			      </a>
			    </div>

			    <div class="col-sm-9 centersmall top10">
			      <strong><?= $Product->getModel(); ?></strong> <br />
			      <?= $Format->formatCurrency($Product->getPrice()); ?> <br />
			      <p>This is a quick description of the product.  Some interesting uses of it should go here.</p>
			      <a class="btn btn-primary btn-md hidden-xs listbut" role="button" href="/product/<?= $Product->getId()."/".$Product->getUrl_slug();?>">View Info</a>
			    </div>
			</div>

			<div class="clearfix visible-*"></div>
  			<hr class="visible-xs" />

  			<? endforeach; ?>

		</div><!-- end container div -->

	</body>

</html>
