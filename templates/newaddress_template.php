<html>
<head>
<base href="http://shoppingcart:8888">	
<style>
table td { border: 1px solid black; }
</style>
</head>

<body>

<? include('header.php'); ?>

<h1>Add new address</h1>

<? if(isset($_SESSION['cart_alert'])) {
echo"<div class=\"alert alert-warning\" id=\"cart-alert\">".$_SESSION['cart_alert']."</div>";
}
?>
		
<form method="POST" action="/cart/storeaddress">
	
		<p>Address information</p>
		<label for='first'>First</label><input type='text' name='first' id='first'> <br />
		<label for='last'>Last</label><input type='text' name='last' id='last'><br />
		<label for='company'>Company</label><input type='text' name='company' id='company'><br />
		<label for='address1'>Address 1</label><input type='text' name='address1' id='address1'><br />
		<label for='address2'>Address 2</label><input type='text' name='address2' id='address2'><br />
		<label for='city'>City</label><input type='text' name='city' id='city'><br />
		<label for='state'>State</label><input type='text' name='state' id='state'><br />
		<label for='zip'>Zip</label><input type='text' name='zip' id='zip'><br />
		<label for='address_type'>Address type</label>
		<select name='address_type'>
			<option value="b">Billing Address</option>
			<option value="s">Shipping Address</option>
		</select> <br />
		<input type="submit" name="submit" value="Add Address">
</form>
	


</body>


</html>
<? unset($_SESSION['cart_alert']); ?>
