<!DOCTYPE html>
<html>

	<head>
	    <title>Cart Checkout</title>
		<? include('head_links_template.php'); ?>
		<style>
		table td { border: 1px solid gray; }
		</style>
	</head>

<div class="container">

<? include('nav_template.php'); ?>

<h1>Cart Checkout Page</h1>

<? if(isset($_SESSION['cart_alert'])) {
echo"<div class=\"alert alert-warning\" id=\"cart-alert\">".$_SESSION['cart_alert']."</div>";
}
?>

<table width=60%>
	<tr><td>Item</td><td>Unit Price</td><td>Quantity</td><td>Line Total</td></tr>
<?
foreach ($cartRowArray as $CartRow):  
?>

<tr><td><?= $CartRow->getModel();?>
	<form method="POST" action="/cart/changeqty">
		<input type="text" name="qty" value="<?= $CartRow->getQty();?>">
		<input type="hidden" name="product_id" value="<?= $CartRow->getProduct_Id();?>">
		<input style="display: inline;" type="submit" name="submit" value="Update">
	</form>
	<form method="POST" action="/cart/delete">
		<input type="hidden" name="product_id" value="<?= $CartRow->getProduct_Id();?>">
		<input style="display: inline;" type="submit" name="submit" value="Delete">
	</form>
	</td>
	<td><?= $Format->formatCurrency($CartRow->getPrice());?></td>
	<td><?= $CartRow->getQty();?></td>
	<td><?= $Format->formatCurrency($CartRow->getLine_Total());?></td>
</tr>

<? endforeach; ?> 


<tr><td colspan=2></td><td>Subtotal</td><td><?= $Format->formatCurrency($CartRepository->loadSubtotal()); ?></td></tr>
<tr><td colspan=2></td><td>Tax</td>
	<td><? if(isset($_SESSION['zip'])) { echo $Format->formatCurrency($CartRepository->loadTax()); } else echo"Enter zip code"; ?></td></tr>
<tr><td colspan=2></td><td>Shipping</td>
	<td>
		<? 
		if ( $CartRepository->loadSubtotal() > 0) { 
			if($CartRepository->loadShipping() != null) { 
				echo $Format->formatCurrency($CartRepository->loadShipping()); 
			}
				else { echo"Enter zip code"; }
			}
			else { echo"$0.00"; }
			
		?>
	</td></tr>
<tr><td colspan=2></td><td>Total</td>
	<td>
		<?
		if ( $CartRepository->loadSubtotal() > 0) { 
			if($CartRepository->loadShipping() != null) { 
				echo $Format->formatCurrency($CartRepository->loadTotal()); 
			}
				else { echo"Enter zip code"; }
			}
			else { echo"$0.00"; }
			
		?>

	</td></tr>
</table>

<p>zip code: <? if(isset($_SESSION['zip'])) { echo $_SESSION['zip']; } ?></p>

<form method="POST" action="/cart/setzip">
	<input type="text" name="zip">
	<input type="submit" name="submit" value="Set Zip">
</form>

<p>If you are an existing user, you may login here:</p>

<form method="post" action="/authorize" />
	<input type="text" name="contact_email" />
	<input type="password" name="password" />
	<input type="hidden" name="return_path" value="/cart" />
	<input type="submit" value="submit" name="submit" />
</form>
		
<form method="POST" action="/cart/storeorder">
	
	<div style="float: left;">
	<p>Shipping information</p>
	<label for='s_first'>First</label><input type='text' name='s_first' id='s_first'> <br />
	<label for='s_last'>Last</label><input type='text' name='s_last' id='s_last'><br />
	<label for='s_company'>Company</label><input type='text' name='s_company' id='s_company'><br />
	<label for='s_address1'>Address 1</label><input type='text' name='s_address1' id='s_address1'><br />
	<label for='s_address2'>Address 2</label><input type='text' name='s_address2' id='s_address2'><br />
	<label for='s_city'>City</label><input type='text' name='s_city' id='s_city'><br />
	<label for='s_state'>State</label><input type='text' name='s_state' id='s_state'><br />
	<label for='s_zip'>Zip</label><input type='text' name='s_zip' id='s_zip' value='<? if(isset($_SESSION['zip'])) { echo $_SESSION['zip']; }?>' DISABLED><br />
	</div>

	<div style="float: left;">
	<p>Billing information</p>
	<label for='b_first'>First</label><input type='text' name='b_first' id='b_first'><br />
	<label for='b_last'>Last</label><input type='text' name='b_last' id='b_last'><br />
	<label for='b_company'>Company</label><input type='text' name='b_company' id='b_company'><br />
	<label for='b_address1'>Address 1</label><input type='text' name='b_address1' id='b_address1'><br />
	<label for='b_address2'>Address 2</label><input type='text' name='b_address2' id='b_address2'><br />
	<label for='b_city'>City</label><input type='text' name='b_city' id='b_city'><br />
	<label for='b_state'>State</label><input type='text' name='b_state' id='b_state'><br />
	<label for='b_zip'>Zip</label><input type='text' name='b_zip' id='b_zip'><br />
	<label for='contact_name'>Contact Name</label><input type='text' name='contact_name' id='contact_name'><br />
	<label for='contact_email'>Email</label><input type='text' name='contact_email' id='contact_email'><br />
	<label for='contact_phone'>Phone</label><input type='text' name='contact_phone' id='contact_phone'><br /><br />

	<label for='password'>Create a Password</label><input type='password' name='password' id='password'><br /><br />
	
	<!-- need to pass the shipping zip code hidden since the DISABLED field above won't post -->
	<input type='hidden' name='s_zip' value='<? if(isset($_SESSION['zip'])) { echo $_SESSION['zip']; }?>'>
	<input type="submit" name="submit" value="Place Order">
</form>
	<div style="float: left;">

</div> <!-- end container -->
</body>


</html>
<? unset($_SESSION['cart_alert']); ?>
