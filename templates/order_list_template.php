<html>

<head>
<title>Order List</title>
</head>

<body>

<? include('header.php'); ?>

<H1>Order List</H1>

<table>
	<tr><td>Order #</td><td>Order Date</td><td>Total</td><td>Tracking</td></tr>

<? foreach ($orderListArray as $Order):
?>

<tr><td><a href="/order/view/<?= $Order->getOrder_id();?>">Order # <?= $Order->getOrder_id();?></a></td>
	<td><?= $Order->getOrder_date();?></td>
	<td><?= $Format->formatCurrency($Order->getTotal());?></td>
	<td>
		<form method="post" action="/nquinn/shoppingcart/order/setstatus">
			<input type="hidden" name="order_id" value="<?= $Order->getOrder_id();?>">
			<input type="submit" name="submit" value="Set Tracking">
			<input type="text" name="track" value="<?= $Order->getTrack();?>">
		</form>
		</td></tr>

<? endforeach; ?>

</table>

</body>

</html>