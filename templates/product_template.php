<!DOCTYPE html>
<html>

	<head>
	    <title>Corrosion Block - <?= $Product->getModel(); ?></title>
		<? include('head_links_template.php'); ?>
		<? //$pagename = "blah";
			// include('analytics.php'); ?>
	</head>

	<body>

		<div class="container">
			<? include('nav_template.php'); ?>

			<div class="row top10">
			    <div class="col-sm-4 centersmall">
			     	<img width=250 src="<?= $Product->getPrimaryImagePath(); ?>">
			     </div>

			    <div class="col-sm-8 centersmall">
				    <H1><?= $Product->getModel(); ?></H1><br />
				
				    <?=$Format->formatCurrency($Product->getPrice()); ?>

				    <br /><br />
				
				    <?=$Product->getProdInfo(); ?>
		        
		        	<br /><br />

		        	<? if ($Product->getIsPurchasable() == 1): ?>
		        		<form method="POST" action="/cart/add">
		        		<input type="text" name="qty" value="1">
		        		<input type="submit" name="submit" value="Add to Cart">
	        			<input class="btn btn-primary btn-md hidden-xs listbut" type="hidden" name="product_id" value="<?=$Product->getId(); ?>">
		        		</form>
	        		<? else: ?>
	        			<p>Sorry, ths product is not available to order online.  Please call us at 708-474-3739 for an accurate shipping quote.</p>
	        		<? endif; ?>
	        		
			      <!--<a class="btn btn-primary btn-md hidden-xs listbut" role="button" href="/product/<?= $Product->getId()."/".$Product->getUrl_slug();?>">View Info</a>-->
			    </div>
			</div>

			<div class="clearfix visible-*"></div>
  			<hr class="visible-xs" />


		</div><!-- end container div -->

	</body>
</html>



