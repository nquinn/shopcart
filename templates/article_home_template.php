<!DOCTYPE html>
<html>

	<head>
	    <title>Corrosion Block - NoCorrosion.com</title>
		<? include('head_links_template.php'); ?>
	</head>

	<body>
		<div class="container">
		<? include('nav_template.php'); ?>

			<div class="row top10">
			    <div class="col-sm-4 centersmall">
			     	<img width=250 src="/assets/images/cb_group_shot.png">
			     </div>

			    <div class="col-sm-8 centersmall">
				    <H1>Corrosion Block</H1>
				    <p>
				    	Do you need a corrosion control solution? Experts estimate that protecting your equipment now is ten times cheaper than repairing it later. As a bonus, electrical and mechanical parts will run smoother and more efficiently.
					</p>

					<p>
						Corrosion Block is a revolutionary product that: kills corrosion on contact, stops corrosion from ever coming back, and finally lubricates the area.
						Corrosion Block is safe to nearly all electronics.
					</p>

					<p>
						Corrosion block is a marine version of the aviation and
						military's ACF-50.  This is a specially formulated penetrant that
						was created to battle the worst kind of corrosion.  
						Click continue to learn how it works!
					</p>

			    </div>
			</div>



		</div><!-- end container div -->

	</body>
</html>



