<html>
<head>
<!-- <base href="http://shoppingcart:8888" /> -->
<style>
table td { border: 1px solid black; }
</style>
</head>

<body>

<? include('header.php'); ?>

<h1>Cart Checkout Page</h1>

<? if(isset($_SESSION['cart_alert'])) {
echo"<div class=\"alert alert-warning\" id=\"cart-alert\">".$_SESSION['cart_alert']."</div>";
}
?>

<table width=60%>
	<tr><td>Item</td><td>Unit Price</td><td>Quantity</td><td>Line Total</td></tr>
<?
foreach ($cartRowArray as $CartRow):  
?>

<tr><td><?= $CartRow->getModel();?>
	<form method="POST" action="/cart/changeqty">
		<input type="text" name="qty" value="<?= $CartRow->getQty();?>">
		<input type="hidden" name="product_id" value="<?= $CartRow->getProduct_Id();?>">
		<input style="display: inline;" type="submit" name="submit" value="Update">
	</form>
	<form method="POST" action="/cart/delete">
		<input type="hidden" name="product_id" value="<?= $CartRow->getProduct_Id();?>">
		<input style="display: inline;" type="submit" name="submit" value="Delete">
	</form>
	</td>
	<td><?= $Format->formatCurrency($CartRow->getPrice());?></td>
	<td><?= $CartRow->getQty();?></td>
	<td><?= $Format->formatCurrency($CartRow->getLine_Total());?></td>
</tr>

<? endforeach; ?> 


<tr><td colspan=2></td><td>Subtotal</td><td><?= $Format->formatCurrency($CartRepository->loadSubtotal()); ?></td></tr>
<tr><td colspan=2></td><td>Tax</td>
	<td><? if(isset($_SESSION['zip'])) { echo $Format->formatCurrency($CartRepository->loadTax()); } else echo"Enter zip code"; ?></td></tr>
<tr><td colspan=2></td><td>Shipping</td>
	<td>
		<? 
		if ( $CartRepository->loadSubtotal() > 0) { 
			if($CartRepository->loadShipping() != null) { 
				echo $Format->formatCurrency($CartRepository->loadShipping()); 
			}
				else { echo"Please enter a valid zip code"; }
			}
			else { echo"$0.00"; }
			
		?>
	</td></tr>
<tr><td colspan=2></td><td>Total</td>
	<td>
		<?
		if ( $CartRepository->loadSubtotal() > 0) { 
			if($CartRepository->loadShipping() != null) { 
				echo $Format->formatCurrency($CartRepository->loadTotal()); 
			}
				else { echo"Please enter a valid zip code"; }
			}
			else { echo"$0.00"; }
			
		?>

	</td></tr></table>
<p><a href="/cart/newaddress">Add a new address</a>, or use a recent one:</p>
<div style="float: left; margin-right: 50px;">
	<p>Choose your billing address</p>

	<div class="addressbox">
	<? 
	foreach ($billing_address_array as $Billing_address) {

		if ($User->getBillAddress_id() == $Billing_address->getAddress_id()) {
			echo"<img width=20 src=\"../assets/images/check-glyph.png\" />";
		}

		echo
		$Billing_address->getFirst()." ".$Billing_address->getLast()."<br />".
		$Billing_address->getCompany()."<br />".
		$Billing_address->getAddress1()."<br />";
			if ($Billing_address->getAddress2() != null) {
			echo $Billing_address->getAddress2()."<br />";
		}
		echo $Billing_address->getCity().", ".$Billing_address->getState()."<br />".
		$Billing_address->getZip()."<br />";

		echo"
		<form method=\"POST\" action=\"/cart/setbilladdress\">
		<input type=\"hidden\" name=\"address_id\" value=\"".$Billing_address->getAddress_id()."\">
		<input type=\"submit\" name=\"submit\" value=\"Use this Billing Address\">
		</form> 
		";
	}
	?>
	</div>
</div>

<div style="float: left;">
	<p>Choose your shipping address</p>

	<div class="addressbox">
	<? 
	foreach ($shipping_address_array as $Shipping_address) {

		if ($User->getShipAddress_id() == $Shipping_address->getAddress_id()) {
			echo"<img width=20 src=\"../assets/images/check-glyph.png\" />";
		}

		echo
		$Shipping_address->getFirst()." ".$Shipping_address->getLast()."<br />".
		$Shipping_address->getCompany()."<br />".
		$Shipping_address->getAddress1()."<br />";
		if ($Shipping_address->getAddress2() != null) {
			echo $Shipping_address->getAddress2()."<br />";
		}
		echo $Shipping_address->getCity().", ".$Billing_address->getState()."<br />".
		$Shipping_address->getZip()."<br />";

		echo"
		<form method=\"POST\" action=\"/cart/setshipaddress\">
		<input type=\"hidden\" name=\"address_id\" value=\"".$Shipping_address->getAddress_id()."\">
		<input type=\"submit\" name=\"submit\" value=\"Use this Shipping Address\">
		</form> 
		";
	}
	?>
	</div>
</div>

<div style="clear: both;" />

<form method="POST" action="/cart/storeorder">
	<input type="submit" name="submit" value="Place Order">
</form>

</body>


</html>
<? unset($_SESSION['cart_alert']); ?>
