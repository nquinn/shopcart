<!DOCTYPE html>
<html>

	<head>
	    <title>Terms and Condition</title>
		<? include('head_links_template.php'); ?>
	</head>

	<body>
		<div class="container">

			<? include('nav_template.php'); ?>

			<H1>Terms and Conditions</H1>

			<p>
				Shipping and handling fees are added at checkout. All shipping goes through UPS ground or USPS, depending on weight. If you are in need of a product sooner, email us for arrangements and pricing.
      		</p>
        	
        	<p>
        		Our return policy:
        	</p>

      		<p>
	        	You have -30 days- to return or exchange a defective product. NoCorrosion sales associates have the final decision for all refunds and exchanges.
	      		</p>

        	<p>
	        	The shipping costs involved in returning a product are 
	          	shared between the customer and Midwest Corrosion Products as follows:
	      	</p>

        	<p>
        		Refunds: Customer ships product back at his or her expense. The purchase price minus initial shipping and merchant account fees will be refunded.  A 20% restocking fee will be applied to all refunds.  Distributors and resellers of Corrosion Block, please contact us directly for separate refund policies.
      		</p>
			
			<p>
				Exceptions:  Refunds are only applicable for full case orders. Single can orders do not apply. Items must be in good condition and in original packaging when returned. All decisions will be at the discretion of Midwest Corrosion Products.
			</p>

        	<p>
        		Exchanges: Customer ships product back at his or her expense. Midwest Corrosion Products will ship a replacement back at our expense.
        	</p>

        	<p>
        		Pricing errors:
        	</p>
        
        	<p>
        		Midwest Corrosion Products is not responsible for any pricing errors 
         		on the web site. Prices are subject to change. 
      		</p>

      		<p>
      			Shipping prices are for the continental United States only. Overseas orders, please call.  Contact us directly for up to date information.
        	</p>

		</div><!-- end container div -->

	</body>
</html>



