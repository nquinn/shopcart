<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />
	<title><?= $Article->getTitle(); ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="../templates/css/images/favicon.ico" />
	<link rel="stylesheet" href="../templates/css/style.css" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>
	
	<script src="../templates/js/jquery-1.8.0.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
		<script src="js/modernizr.custom.js"></script>
	<![endif]-->
	<script src="../templates/js/jquery.carouFredSel-5.5.0-packed.js" type="text/javascript"></script>
	<script src="../templates/js/functions.js" type="text/javascript"></script>
</head>
<body>
<!-- wrapper -->
<div id="wrapper">
	<!-- shell -->
	<div class="shell">
		<!-- container -->
		<div class="container">
			<!-- header -->
			
			<?php include('header_template.php'); ?>
			

			<!-- end of navigation -->
			

			<!-- main -->
			<div class="main">

				<div class="heading">
				<H1><?= $Article->getTitle(); ?></H1>
				<p><?= $Article->getArticle_date(); ?></p>
				</div>

				<div class="content">

				


				<!-- main unformatted content here -->
				<?= $Article->getContent(); ?>
				<!-- end main unformatted content -->



				
				</div>

				<div class="featured">
					<h4>Welcome to <strong>Company Name.</strong> Start Creating Your Website Today completely for <strong>FREE!</strong></h4>
					<a href="#" class="blue-btn">GET IN TOUCH</a>
				</div>

				<section class="cols">
					<div class="col">
						<h3>About Us</h3>
						<h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dui ipsum, cursus ut adipiscing porta, vestibulum quis turpis. Ut ultricies rutrum lorem, in blandit tortor congue pulvinar lorem ipsum dolor sit amet, consectetur adipiscing elit. <br /><a href="#" class="more">view more</a></p>
					</div>

					<div class="col">
						<h3>We’re Hiring</h3>
						<img src="../templates/css/images/col-img.png" alt="" class="left"/>
						<h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h5>
						<div class="cl">&nbsp;</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dui ipsum, cursus ut adipiscing porta, vestibulum quis turpis adispicing amet sit. <br /><a href="#" class="more">view more</a></p>
					</div>

					<div class="col">
						<h3>Our Services</h3>
						<ul>
							<li><a href="#">Lorem ipsum dolor sit amet</a></li>
							<li><a href="#">Sit atmet, consectetur lorem </a></li>
							<li><a href="#">Consectetur adispicing dolor</a></li>
							<li><a href="#">Lipsuim dolor amet adpispicing</a></li>
							<li><a href="#">Lipsuim dolor amet adpispicing</a></li>
						</ul>
					</div>
					<div class="cl">&nbsp;</div>
				</section>

				<section class="entries">
					<div class="entry">
						<h3>Latest Blog Posts</h3>
						<div class="entry-inner">
							<div class="date">
								<strong>01</strong>
								<span>2012</span>
								<em>feb</em>
							</div>
							<div class="cnt">
								<p><a href="#">Lorem ipsum dolor sit<br /> amet, consectetur dor</a></p>
								<p class="meta"><a href="#">by John Doe </a> /  <a href="#">Category Name</a></p>
							</div>
						</div>
						<div class="entry-inner">
							<div class="date">
								<strong>28</strong>
								<span>2012</span>
								<em>jan</em>
							</div>
							<div class="cnt">
								<p><a href="#">Lorem ipsum dolor sit<br /> amet, consectetur dor</a></p>
								<p class="meta"><a href="#">by John Doe </a> /  <a href="#">Category Name</a></p>
							</div>
						</div>
						<div class="entry-inner">
							<div class="date">
								<strong>11</strong>
								<span>2012</span>
								<em>feb</em>
							</div>
							<div class="cnt">
								<p><a href="#">Lorem ipsum dolor sit<br /> amet, consectetur dor</a></p>
								<p class="meta"><a href="#">by John Doe </a> /  <a href="#">Category Name</a></p>
							</div>
						</div>
					</div>
					<div class="entry">
						<h3>Latest Project</h3>
						<h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </h5>
						<a href="#"><img src="../templates/css/images/col-img2.png" alt="" /></a>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dui ipsum, cursus ut adipiscing porta, vestibulum quis turpis adispicing amet sit.  <br /><a href="#" class="more">view more</a></p>
					</div>
					<div class="entry">
						<h3>Testimonials</h3>

						<div class="testimonials">					
							<p><strong>“</strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dui ipsum, cursus ut adipiscing porta, vestibulum quis turpis.”</p>
							<p class="author">John Doe, <strong>Company Name</strong></p>
						</div>
						
						<div class="partners">
							<h3>Our Partners</h3>
							<img src="../templates/css/images/partners-img.png" alt="" />
						</div>
					</div>
					<div class="cl">&nbsp;</div>
				</section>
			</div>
			<!-- end of main -->
			<div class="cl">&nbsp;</div>
			
			<!-- footer -->
			
			<?php include('footer_template.php'); ?>

			<!-- end of footer -->
		</div>
		<!-- end of container -->
	</div>
	<!-- end of shell -->
</div>
<!-- end of wrapper -->
</body>
</html>