<?php

namespace controllers;

class UserController {
	
	private $Pdo;
    
    public function __construct(\PDO $Pdo) {
        $this->Pdo = $Pdo;
    }

    public function authenticateUser() {

    	// NPQTODO - purify these

    	$contact_email = $_POST['contact_email'];
    	$password = $_POST['password'];
    	$return_path = $_POST['return_path'];

    	$UserRepository = new \models\UserRepository($this->Pdo);
    	$User = $UserRepository->authenticateUser($contact_email,$password);
    	
        
    	if (isset($User) && $User->getAuth_level() > 0) {
            header('Location: '.URLBASE.$return_path,TRUE,303);
    	}
        else {
			$login_message = "Invalid user/password combination, please try again.";
    		$this->showAuthForm($return_path,$login_message);
            //$ErrorObject->handleAuthError(...);
    	}

    }

    public function showAuthForm($return_path,$login_message) {
    	include(__DIR__.'/../templates/login_template.php');
    }

    public function showPrivilegeWarning($return_path) {
        $login_message = "Sorry, you don't have sufficient privileges to view this page";
        include(__DIR__.'/../templates/login_template.php');   
    }

    public function resetPassword() {
        $random_code = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $UserRepository = new \models\UserRepository($this->Pdo);
        $UserRepository->resetPassword($random_code);
        include(__DIR__.'/../templates/reset_password_template.php');      
    }

    public function newPasswordForm($random_code) {
        $UserRepository = new \models\UserRepository();
        if ($random_code)
        include(__DIR__.'/../templates/new_password_template.php');      
    }

    public function saveNewPassword(){
        $random_code = $_POST['random_code'];
        $new_password = $_POST['new_password'];
        $user_id = (int) $_POST['user_id'];

    }

    public function logOut() {
        session_unset();
    }

}

