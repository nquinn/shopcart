<?php

namespace controllers;

class CartController {

	private $Pdo;
    
    public function __construct(\PDO $Pdo) {
        $this->Pdo = $Pdo;
    }

    public function addProduct() {

        $session_id = session_id();
    	
    	$product_id = (int) $_POST['product_id'];
    	$qty = (int) $_POST['qty'];

        $CartRepository = new \models\CartRepository($this->Pdo);
    	//if ($tempproduct->checkItem($product_id)) {
        $CartRepository->addProduct($product_id,$qty,$session_id);
        //}
        //NPQTODO need to check item here
    	header('Location: '.URLBASE."/cart",TRUE,303);

    }

    public function deleteProduct() {

        $session_id = session_id();
    	
    	$product_id = (int) $_POST['product_id'];

    	$CartRepository = new \models\CartRepository($this->Pdo);
    	$CartRepository->deleteProduct($product_id,$session_id);
        
    	header('Location: '.URLBASE."/cart",TRUE,303);

    }

    public function changeQty() {

        $session_id = session_id();
    	
    	$product_id = (int) $_POST['product_id'];
    	$qty = (int) $_POST['qty'];

        $CartRepository = new \models\CartRepository($this->Pdo);
    	$CartRepository->changeQty($product_id,$qty,$session_id);
    	
    	header('Location: '.URLBASE."/cart",TRUE,303);

    }

    public function loadLineItems() { 

        $session_id = session_id();

        $CartRepository = new \models\CartRepository($this->Pdo);
        $cartRowArray = $CartRepository->loadLineItems($session_id);

        $Format = new \models\Format();

        // check if user is logged in, show appropriate template
        $User = new \models\User();
        
        if ($User->getAuth_level()) { // show known addresses
            $OrderRepository = new \models\OrderRepository($this->Pdo); 
            $customer_id = $User->getCustomer_id();  
            $billing_address_array = $OrderRepository->loadBillingAddresses($customer_id);
            $shipping_address_array = $OrderRepository->loadShippingAddresses($customer_id);
            include('templates/cart_loggedin_template.php');
        }

        else { // guest style login with full form
     
            $return_path = urlencode('/cart');
            $login_message = urlencode('Please login to access your account.');
    		include('templates/cart_template.php');
        }
	}

	public function saveZip($zip) {

        $User = new \models\User();
        $User->setZip($_POST['zip']);
        
		header('Location: '.URLBASE."/cart",TRUE,303);
	}

    public function newAddress() {
        $User = new \models\User();
        include('templates/newaddress_template.php');
    }

    public function storeAddress() {

        $config = \HTMLPurifier_Config::createDefault();
        $purifier = new \HTMLPurifier($config);

 
        $first = $purifier->purify($_POST['first']);
        $last = $purifier->purify($_POST['last']);
        $company = $purifier->purify($_POST['company']);
        $address1 = $purifier->purify($_POST['address1']);
        $address2 = $purifier->purify($_POST['address2']);
        $city = $purifier->purify($_POST['city']);
        $state = $purifier->purify($_POST['state']);
        $zip = $purifier->purify($_POST['zip']);

        $address_type = $purifier->purify($_POST['address_type']);

        if ($address_type == 'b') {
            $addressarray = array(

                    'b_first' => $first,
                    'b_last' => $last,
                    'b_company' => $company,
                    'b_address1' => $address1,
                    'b_address2' => $address2,
                    'b_city' => $city,
                    'b_state' => $state,
                    'b_zip' => $zip
                    );
        }
        if ($address_type == 's') {
            $addressarray = array(

                    's_first' => $first,
                    's_last' => $last,
                    's_company' => $company,
                    's_address1' => $address1,
                    's_address2' => $address2,
                    's_city' => $city,
                    's_state' => $state,
                    's_zip' => $zip
                    );
        }

        $User = new \models\User();
        $customer_id = $User->getCustomer_id();

        $CartRepository = new \models\CartRepository($this->Pdo);

        if ($address_type == 's') {
            $address_id = $CartRepository->saveCustomerShipAddress($customer_id,$addressarray);
            $User->setShipAddress_id($address_id);
            $User->setZip($zip);
        }
        if ($address_type == 'b') {
            $address_id = $CartRepository->saveCustomerBillAddress($customer_id,$addressarray);   
            $User->setBillAddress_id($address_id);
        }

        $_SESSION['cart_alert'] = 'A new address has been added.';
        
        header('Location: '.URLBASE."/cart",TRUE,303);

    }

    public function saveBillAddress() {

        // just sets the selected billing address to 
        // NPQTODO: security, a manipulated post here could expose another
        // users address

        $User = new \models\User();
        $address_id = (int) $_POST['address_id'];
        $User->setBillAddress_id($_POST['address_id']);

        $OrderRepository = new \models\OrderRepository($this->Pdo);
        $Address = $OrderRepository->loadAddress($address_id);
        
        header('Location: '.URLBASE."/cart",TRUE,303);
    }

    public function saveShipAddress() {

        $User = new \models\User();
        $address_id = (int) $_POST['address_id'];
        $User->setShipAddress_id($address_id);

        // also set the zip code in session for calculation purposes

        $OrderRepository = new \models\OrderRepository($this->Pdo);
        $Address = $OrderRepository->loadAddress($address_id);
        $zip = $Address->getZip();
        $User->setZip($zip);
        
        header('Location: '.URLBASE."/cart",TRUE,303);
    }

    public function saveOrder() {

        $User = new \models\User();

        // if this is a guest checkout, receive these as POST's

        if (!$User->getAuth_level()) {
            $config = \HTMLPurifier_Config::createDefault();
            $purifier = new \HTMLPurifier($config);

            $s_first = $purifier->purify($_POST['s_first']);
            $s_last = $purifier->purify($_POST['s_last']);
            $s_company = $purifier->purify($_POST['s_company']);
            $s_address1 = $purifier->purify($_POST['s_address1']);
            $s_address2 = $purifier->purify($_POST['s_address2']);
            $s_city = $purifier->purify($_POST['s_city']);
            $s_state = $purifier->purify($_POST['s_state']);
            $s_zip = $purifier->purify($_POST['s_zip']);
            $b_first = $purifier->purify($_POST['b_first']);
            $b_last = $purifier->purify($_POST['b_last']);
            $b_company = $purifier->purify($_POST['b_company']);
            $b_address1 = $purifier->purify($_POST['b_address1']);
            $b_address2 = $purifier->purify($_POST['b_address2']);
            $b_city = $purifier->purify($_POST['b_city']);
            $b_state = $purifier->purify($_POST['b_state']);
            $b_zip = $purifier->purify($_POST['b_zip']);
            $contact_name = $purifier->purify($_POST['contact_name']);
            $contact_email = $purifier->purify($_POST['contact_email']);
            $contact_phone = $purifier->purify($_POST['contact_phone']);
            $password = $_POST['password'];


            if ($User->getZip() != $s_zip) { 
                echo "<p>session zip: ".$_SESSION['zip']." POST'ed zip: $s_zip";
                die("Your zip code must match the one you set for a shipping estimate.");
            }

            $customerarray = array(

                's_first' => $s_first,
                's_last' => $s_last,
                's_company' => $s_company,
                's_address1' => $s_address1,
                's_address2' => $s_address2,
                's_city' => $s_city,
                's_state' => $s_state,
                's_zip' => $s_zip,
                'b_first' => $b_first,
                'b_last' => $b_last,
                'b_company' => $b_company,
                'b_address1' => $b_address1,
                'b_address2' => $b_address2,
                'b_city' => $b_city,
                'b_state' => $b_state,
                'b_zip' => $b_zip,
                'contact_name' => $contact_name,
                'contact_email' => $contact_email,
                'contact_phone' => $contact_phone,
                'password' => $password

                );

            $CartRepository = new \models\CartRepository($this->Pdo);
            $CartInfo = new \models\CartInfo();
            $order_id = $CartRepository->saveOrder($customerarray,$CartInfo);

        } // end saving order if user is not logged in


        elseif ($User->getAuth_level()) {
            $CartRepository = new \models\CartRepository($this->Pdo);
            $CartInfo = new \models\CartInfo();
            $order_id = $CartRepository->saveOrderLoggedIn($CartInfo);
        }

        //$CartRepository->clearCart(); // need to clear the temptable
        // NPQTODO

        // get merchant from config.ini
        $configarray = parse_ini_file(__DIR__.'/../config.ini');
        $merchant = $configarray['merchant'];

        $User = new \models\User();
        $User->setOrder_id($order_id);

        if ($merchant == 'quantum') {
            header('Location: '.URLBASE."/cart/pay/quantum",TRUE,303);
        }
        elseif ($merchant == 'authorize') {
            header('Location: '.URLBASE."/cart/pay/authorize",TRUE,303);
        }
        
    }

    public function orderPaymentQuantum() {

    if(!isset($_SESSION['order_id'])) { 
        throw new \Exception ("Sorry, payment options only available if you check out from our shopping cart");
    }
    
    $User = new \models\User();
    $order_id = $User->getOrder_id();

    $OrderRepository = new \models\OrderRepository($this->Pdo);
    $OrderSummary = $OrderRepository->loadOrderSummary($order_id);
    $Billing_address = $OrderRepository->loadOrderBillAddress($order_id);
    $Shipping_address = $OrderRepository->loadOrderShipAddress($order_id);

    $configarray = parse_ini_file(__DIR__.'/../config.ini');
    $merchantmode = $configarray['merchantmode']; // test or production

    if ($configarray['merchantmode'] == 'localtest') {
        $API_UNAME = $configarray['quantum_test_login'];
        $API_KEY = $configarray['quantum_test_key'];
    }
    if ($configarray['merchantmode'] =='production') {
        $API_UNAME = $configarray['quantum_prod_login'];
        $API_KEY = $configarray['quantum_prod_key'];  
    }

    // note that for testing on the test.domain.com server we would have to
    // manually set the response_url in the admin interface.
    // I don't believe there's a "test mode" for quantum in prod

    include_once(__DIR__."/../inc/quantum_iframe.php");

    // cleanup phone number
    //$phone = $OrderSummary->getContact_phone();
    //$remove = array("(",")",".","-");
    //$phone = str_replace(""

    // this builds our <iframe> code in an array (script and iframe), need both
    $quantum = quantumilf_getCode($API_UNAME, $API_KEY, '800', '800', 
    $OrderSummary->getTotal(), $OrderSummary->getOrder_id(), $OrderSummary->getCustomer_id(), 
    '0', 'Y', 'Y',
    $Billing_address->getFirst(), $Billing_address->getLast(),$Billing_address->getAddress1(),
    $Billing_address->getAddress2(),$Billing_address->getCity(),$Billing_address->getState(),$Billing_address->getZip(),$OrderSummary->getContact_email());

    include(__DIR__."/../templates/order_payment_quantum_template.php");
    
    }

    public function orderPaymentAuthorize() {

    if(!isset($_SESSION['order_id'])) { 
        throw new \Exception ("Sorry, payment options only available if you check out from our shopping cart");
    }

    $User = new \models\User();
    $order_id = $User->getOrder_id();

    $OrderRepository = new \models\OrderRepository($this->Pdo);
    $OrderSummary = $OrderRepository->loadOrderSummary($order_id);
    $Billing_address = $OrderRepository->loadOrderBillAddress($order_id);
    $Shipping_address = $OrderRepository->loadOrderShipAddress($order_id);
    $Format = new \models\Format();

    require_once __DIR__.'/../inc/anet_php_sdk/AuthorizeNet.php'; // Include the SDK you downloaded in Step 2

    // current admin tool login info:  authguy123/Myauth123 for sandbox
    
    $configarray = parse_ini_file(__DIR__.'/../config.ini');
    // comment this out later
    $merchantmode = $configarray['merchantmode']; // test or production

    echo"<p>merchantmode: $merchantmode</p>";

    if ($configarray['merchantmode'] == 'localtest') {
        // test id's
        $api_login_id = $configarray['auth_test_login'];
        $transaction_key = $configarray['auth_test_trans_key'];
        $auth_url = $configarray['auth_test_url'];
        $response_url = $configarray['auth_test_response_url'];
    }
    if ($configarray['merchantmode'] == 'servertest') {
        // test id's
        $api_login_id = $configarray['auth_test_login'];
        $transaction_key = $configarray['auth_test_trans_key'];
        $auth_url = $configarray['auth_test_url'];
        $response_url = $configarray['auth_prod_test_response_url'];
    }
    if ($configarray['merchantmode'] == 'productiontest') {
        // real id's
        $api_login_id = $configarray['auth_prod_login'];
        $transaction_key = $configarray['auth_prod_trans_key'];
        $auth_url = $configarray['auth_prod_url'];
        $response_url = $configarray['auth_prod_response_url'];
        $test_request = '<INPUT TYPE="HIDDEN" NAME="x_test_request" VALUE="TRUE">';
    }
    if ($configarray['merchantmode'] == 'production') {
        // real id's
        $api_login_id = $configarray['auth_prod_login'];
        $transaction_key = $configarray['auth_prod_trans_key'];
        $auth_url = $configarray['auth_prod_url'];
        $response_url = $configarray['auth_prod_response_url'];
    }

    $amount = $Format->formatDollars($OrderSummary->getTotal()); 
    $fp_sequence = $order_id . time(); // Enter an invoice or other unique number.
    $fp_timestamp = time();
    $fingerprint = \AuthorizeNetSIM_Form::getFingerprint($api_login_id,$transaction_key, $amount, $fp_sequence, $fp_timestamp);

    include(__DIR__."/../templates/order_payment_authorize_template.php");
    
    }

    public function viewBoxes() {  // this method is only here for debugging, delete later
        $CartRepository = new \models\CartRepository($this->Pdo);
        $box_weights = $CartRepository->shippingLoadBoxWeights();
        $handling = $CartRepository->shippingLoadHandling($box_weights);
        echo"<pre>".var_dump($box_weights)."</pre>";
        echo"<p>handling: $handling</p>";
    }

    public function checkFilter() { // also used for debugging only

        $config = \HTMLPurifier_Config::createDefault();
        $purifier = new \HTMLPurifier($config);

        $s_first ='<script src="http://www.evilurl.com"></script>Clean Stuff Here';
        $s_first_clean = $purifier->purify($s_first);

        echo"clean: $s_first_clean";

    }


}



