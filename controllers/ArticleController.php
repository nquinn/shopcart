<?php

namespace controllers;

class ArticleController {

	private $Pdo;

    public function __construct(\PDO $Pdo) {
        $this->Pdo = $Pdo;
    }

    public function loadArticle($article_id) {
    	
    	$Article = new \models\ArticleRepository($this->Pdo);
    	$Article->getArticle($article_id);

    	include(__DIR__.'/../templates/article_template.php');

    }

    public function loadHome() {
    	
    	include(__DIR__.'/../templates/article_home_template.php');

    }

    public function loadTerms() {
        include(__DIR__.'/../templates/article_terms_template.php');
    }

    public function loadContact() {
        include(__DIR__.'/../templates/article_contact_template.php');
    }

    public function sendContact() {
        
        $from = $_POST['from'];
        $message = $_POST['message'];
        mail($to,$from,$message);

        $contactmessage = 'Thank you, your message has been sent to us';
        header('Location: '.URLBASE."/contact",TRUE,303);
    }

}

