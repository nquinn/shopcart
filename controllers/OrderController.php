<?php

namespace controllers;


class OrderController {

	private $Pdo;
    
    public function __construct(\PDO $Pdo) {
        $this->Pdo = $Pdo;
    }

	public function loadOrder($order_id) {

		$User = new \models\User();
		$OrderRepository = new \models\OrderRepository($this->Pdo);
		
		$OrderSummary = $OrderRepository->loadOrder($order_id);
			$Billing_address = $OrderSummary->getBilling_address();
			$Shipping_address = $OrderSummary->getShipping_address();
		$OrderItems = $OrderRepository->loadOrderItems($order_id);
		$Format = new \models\Format();

		if ($OrderSummary->getCustomer_id() == $User->getCustomer_id() || $User->getAuth_level() == 2) {
			include(__DIR__.'/../templates/order_template.php');
		}
		else {
			$UserController = new \controllers\UserController($this->Pdo);
			//$AuthLevel->showPrivilegeWarning();
			echo"<p>Sorry, this order belongs to a different customer.</p>";
		}

	}

	public function saveMerchantResponseAuthorize() { // authorize.net, etc, posts back to this method for validation
		
		$Format = new \models\Format();
		$config = \HTMLPurifier_Config::createDefault();
        $purifier = new \HTMLPurifier($config);

		// authorize stuff here
		$response_code = $purifier->purify($_POST['x_response_code']); // 1 = approved, 2 = declined, 3 =error
		$auth_code = $purifier->purify($_POST['x_auth_code']); // generic transaction auth code for security
		$orderid = (int) $_POST['x_invoice_num'];
		$amount = $purifier->purify($_POST['x_amount']);
			$amount = $Format->formatDollars($amount);
		$authhash = $purifier->purify($_POST['x_MD5_Hash']); // store this with our order fingerprint
		$transid = $purifier->purify($_POST['x_trans_id']); // transaction ID, this shows up in reports
		// optional responses to store:
		//$cvv_code = $_POST['x_cvv2_resp_code']; // cvv response (M=match, N= no match, P = not processed, S = should be present, U = unable to process)
		//$custid = (int) $_POST['x_cust_id'];
		//$lastfour = $_POST['x_account_number'];
		//$avs_code = $_POST['x_avs_code'];  // address verification 
		//$response_reason_code = $_POST['x_response_reason_code']; // 300 variations on why it might have failed

		$mytranshash = strtoupper(md5('plutz345#$%'.'8RcZee4KE5z8'.$transid.$amount));

		if ($mytranshash == $authhash){ // only process response if authenticated
			$OrderRepository = new \models\OrderRepository($this->Pdo);
			$OrderRepository->setMerchantResponse($order_id,$trans_id,$auth_code,$response_code);
			$this->mailReceipt($orderid); // mail off result to customer
		}

		
	}

	public function saveMerchantResponseQuantum() {

		$Format = new \models\Format();

		// check if auth code matches what we pushed before or whatever - i know there
    	// is some kind of hashed security check here

        // here's quantums GET response:
        //?Amount=1220.00
        // &transID=61261187
        // &ID=13
        // &md5_hash=88b41a2d782001ac569f18dca6d318cf
        // &CustomerID=
        // &trans_result=APPROVED

		// need to customize this for quantum
		// Hash value above, gateway login, transaction ID and amount. These are combined together then it is MD5ed.
		// Example: md5(ThisIsMyHashmyTestLogin554124110.00)

		// &decline_reason=AUTH DECLINE
		// &errorcode=200

		$config = \HTMLPurifier_Config::createDefault();
        $purifier = new \HTMLPurifier($config);

		$postedhash = $purifier->purify($_POST['md5_hash']);
		$order_id = (int) $_POST['ID'];
		$trans_result = $purifier->purify($_POST['trans_result']); // APPROVED or DECLINED need to convert this to 1 or 0
		$amount = $purifier->purify($_POST['Amount']);
			$amount = $Format->formatDollars($amount);
		$trans_id = $purifier->purify($_POST['transID']);

		if ($trans_result =='APPROVED') {  // authorize.net uses "repsonse code", mapping it to an int to be similar
		$response_code = 1;  // 1= approved, 2=declined, 3=error
		$this->mailReceipt($order_id); // mail off result to customer
		}
		else { $response_code = 2; } 

		// settings hash pw . gateway login . transaction id . trans_amount

		$hashstring = "neilhash"."quinntst".$trans_id.$amount;
		$myhash = md5($hashstring);


		if ($myhash == $postedhash){ // only process response if authenticated
			$OrderRepository = new \models\OrderRepository($this->Pdo);
			$OrderRepository->setMerchantResponse($order_id,$trans_id,$auth_code,$response_code);
			// write code to email with response_code as argument
		}
		else { die("Authentication hash string does not match"); }

	}

	public function mailReceipt($order_id) {
	
		$OrderRepository = new \models\OrderRepository($this->Pdo);
		$OrderSummary = $OrderRepository->loadOrderSummary($order_id);

		$to = $OrderSummary->getContact_email();
		$subject = "NoCorrosion Order Details";
		$message = "\n
		Thank you for your order from NoCorrosion.com!\n
		You may visit this page to track your order:\n
		http://www.nocorrosion.com/order/status/".$OrderSummary->getOrder_id()." \n
		Please allow 1-2 business days for us to update the page.\n
		If you have any questions, you may contact us at orders@nocorrosion.com.\n
		Your order number is ".$OrderSummary->getOrder_id().".";
		$from_name = "NoCorrosion.com";
		$from_address = "orders@nocorrosion.com";

		mail ($to, $subject, $message, "From:$from_name<$from_address>");


		// email confirmation sent to company
		$to = "orders@nocorrosion.com";
		$subject = "An order ".$OrderSummary->getOrder_id()." has been placed at NoCorrosion.com";
		$message = "An order has been placed: https://www.nocorrosion.com/order/view/".$OrderSummary->getOrder_id();
		$message .= "\nOrder: ".$OrderSummary->getOrder_id()
		."\nCustomer: ".$OrderSummary->getCustomer_id()
		."\nAmount: ".$OrderSummary->getTotal()
		."\nTransaction ID: ".$OrderSummary->getTrans_id()
		."\nResponse Code: ".$OrderSummary->getResponse_code()." (1=approved, 2=declined, 3=error, 4=held)";


		$from_name = "NoCorrosion.com";
		$from_address = "orders@nocorrosion.com";
		mail ($to, $subject, $message, "From:$from_name<$from_address>");

	}

	public function loadOrderStatus($order_id) {

		$OrderRepository = new \models\OrderRepository($this->Pdo);
		$OrderSummary = $OrderRepository->loadOrderSummary($order_id);

		include(__DIR__.'/../templates/order_status_template.php');

	}

	public function saveOrderStatus() {

		$config = \HTMLPurifier_Config::createDefault();
        $purifier = new \HTMLPurifier($config);

    	$order_id = (int) $_POST['order_id'];
		$track = $purifier->purify($_POST['track']);

		$OrderRepository = new \models\OrderRepository($this->Pdo);
		$status_stored = $OrderRepository->saveOrderStatus($order_id,$track);
		if ($status_stored) {
			echo"<p>Tracking code successfully added</p>";
		}

		
	}

	public function listOrders() { // just do 20 most recent for now
		$OrderRepository = new \models\OrderRepository($this->Pdo);
		$orderListArray = $OrderRepository->listOrders();
		$Format = new \models\Format();

		include(__DIR__.'/../templates/order_list_template.php');

	}


}

?>

