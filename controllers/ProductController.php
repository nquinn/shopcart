<?php

// singleproduct - display the properties of a single product

namespace controllers;

class ProductController {

	private $Pdo;
    
    public function __construct(\PDO $Pdo) {
        $this->Pdo = $Pdo;
    }

	public function loadProduct($product_id) {

		$ProductRepository = new \models\ProductRepository($this->Pdo);
		$Product = $ProductRepository->loadProduct($product_id);

		$Format = new \models\Format();
		
		include(__DIR__.'/../templates/product_template.php');

	}

	public function listProducts($category) {


        $config = \HTMLPurifier_Config::createDefault();
        $purifier = new \HTMLPurifier($config);

        $category = $purifier->purify($category);

		$ProductRepository = new \models\ProductRepository($this->Pdo);
		$productarray = $ProductRepository->listProducts($category);

		$Format = new \models\Format();

		include(__DIR__.'/../templates/category_template.php');

	}

}

?>

