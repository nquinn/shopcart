The goal of this project was to learn OOP and best practices. I used PHP because it was a language I am familiar with at the time.

Some notes:

- No framework was used intentionally. I wanted to understand what typically happens behind the scenes in frameworks, especially the front controller.
- Templates use bootstrap to be responsive
- Used PDO for database connections
- Integrated both authorize.net and quantum gateways

The code has not been cleaned or refactored yet. This is mostly an exercise to start understanding the structure of projects.