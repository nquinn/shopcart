<?php
// index.php or the front controller
session_start();
ob_start();

//echo"<pre>".var_dump($_SESSION)."</pre>";

include_once(__DIR__.'/inc/password.php');
$configarray = parse_ini_file('config_kh78hjkh4k34h.ini');
define("URLBASE",$configarray['urlbase']);
define("URLBASESECURE",$configarray['urlbasesecure']);

if ($configarray['db_mode'] == 'test') {
	$login = $configarray['db_test_name'];
	$pass = $configarray['db_test_pass'];
	$mydb = $configarray['db_test_db'];
	$db = new PDO("mysql:host=localhost;dbname=$mydb", "$login", "$pass");
}
elseif ($configarray['db_mode'] == 'production') {
	$login = $configarray['db_prod_name'];
	$pass = $configarray['db_prod_pass'];
	$mydb = $configarray['db_prod_db'];
	$db = new PDO("mysql:host=localhost;dbname=$mydb", "$login", "$pass");
}
elseif ($configarray['db_mode'] == 'localtest') {
	$login = $configarray['db_localtest_name'];
	$pass = $configarray['db_localtest_pass'];
	$mydb = $configarray['db_localtest_db'];
	$db = new PDO("mysql:host=localhost;dbname=$mydb", "$login", "$pass");
}

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$uri = $_SERVER['REQUEST_URI'];
$uri = str_replace("/nquinn/shoppingcart","",$uri);

//set_exception_handler(function(Exception $exception)) {
//	echo var_dump($exception);
//	}


function __autoload($class_name) {
 	
 	$class_name = str_replace("\\","/",$class_name);
 	//echo"<pre>class name: $class_name</pre>";
    include_once $class_name . '.php';  
}

// this needs to be after our own autoloader 
require_once __DIR__.'/inc/htmlpurifier-4.5.0/library/HTMLPurifier.auto.php';

// For maximum performance, make sure that
// library/HTMLPurifier/DefinitionCache/Serializer is writeable by the webserver.

// If you are in the library/ folder of HTML Purifier, you can set the
// appropriate permissions using:

//     chmod -R 0755 HTMLPurifier/DefinitionCache/Serializer


// setup an array of path patterns and matching controller/method/args

$routeList = array(
	'^/$' => array("\controllers\ArticleController","loadHome"),
	'^/product\/(\d+)' => array("\controllers\ProductController","loadProduct"),
	'^/category\/(\w+)' => array("\controllers\ProductController","listProducts"),
	'^/cart$' => array("\controllers\CartController","loadLineItems"),
	'^/cart\/add' => array("\controllers\CartController","addProduct"),
	'^/cart\/delete' => array("\controllers\CartController","deleteProduct"),
	'^/cart\/changeqty' => array("\controllers\CartController","changeQty"),
	'^/cart\/setzip' => array("\controllers\CartController","saveZip"),
	'^/cart\/newaddress' => array("\controllers\CartController","newAddress"),
	'^/cart\/storeaddress' => array("\controllers\CartController","storeAddress"),
	'^/cart\/setbilladdress' => array("\controllers\CartController","saveBillAddress"),
	'^/cart\/setshipaddress' => array("\controllers\CartController","saveShipAddress"),
	'^/cart\/storeorder' => array("\controllers\CartController","saveOrder"),
	'^/cart\/pay\/quantum' => array("\controllers\CartController","orderPaymentQuantum"),
	'^/cart\/pay\/authorize' => array("\controllers\CartController","orderPaymentAuthorize"),
	'^/cart\/boxes' => array("\controllers\CartController","viewBoxes"),
	'^/cart\/checkfilter' => array("\controllers\CartController","checkFilter"),
	'^/order\/view\/(\d+)' => array("\controllers\OrderController","loadOrder"),
	'^/order\/list\/(\d+)' => array("\controllers\OrderController","listOrders"),
	'^/order\/status\/(\d+)' => array("\controllers\OrderController","loadOrderStatus"),
	'^/order\/setstatus' => array("\controllers\OrderController","saveOrderStatus"),
	'^/order\/merchant' => array("\controllers\OrderController","saveMerchantResponse"),
	'^/article\/(\d+)/' => array("\controllers\ArticleController","loadArticle"),
	'^/login\/(.*?)\/(.*?)' => array("\controllers\UserController","showAuthForm"),
	'^/authorize' => array("\controllers\UserController","authenticateUser"),
	'^/resetpass' => array("\controllers\UserController","resetPassword"),
	'^/newpass' => array("\controllers\UserController","newPassword"),
	'^/clear' => array("\controllers\UserController","logOut")
	);

$secured_paths = array(
    	'^/order\/view\/(\d+)' => 1,
    	'^/order\/list\/(\d+)' => 2,
    	'^/cart\/newaddress' => 1,
    	'^/cart\/storeaddress' => 1)
    	;

$User = new \models\User();

foreach($routeList as $path => $dispatch) {


    if(preg_match("#".$path."#", $uri, $matches)) {

  		array_shift($matches); // drop off the first full pattern match
  		if (isset($secured_paths[$path])) {
  		$path_min_auth_level = $secured_paths[$path]; // makes it easier to use isset below
  		}
  		//echo"path min auth level: $path_min_auth_level";

  		if ($User->getAuth_level() == 2) { 
  			// admin, no matter what show the page
  		//	echo"<p>Admin logged in, do whateva</p>";
  			$controller = new $dispatch[0]($db); 
			call_user_func_array(array($controller, $dispatch[1]), $matches); 
  		}
  		elseif ($User->getAuth_level() == 1 && isset($path_min_auth_level) && $path_min_auth_level == 1) {
  			// user is logged in and it's a simple user level page, show it.
  			// let the controller further decide if he has access to the specific component
  		//	echo"<p>User logged in, page access level 1</p>";
  			$controller = new $dispatch[0]($db); 
			call_user_func_array(array($controller, $dispatch[1]), $matches); 	
  		}
  		elseif ($User->getAuth_level() == 1 && isset($path_min_auth_level) && $path_min_auth_level == 2) {
  			// user is logged in, but the page is designed for admins only
  		//	echo"<p>User logged in, but no privilege for this page</p>";
  			$UserController = new \controllers\UserController($db);
  			$UserController->showPrivilegeWarning($uri);
  		}
		elseif (isset($path_min_auth_level) && $path_min_auth_level > 0) {
			// user is not logged in, and the page requires it
		//	echo"<p>User not logged in, but we need them to since page is ok for them</p>";
			$UserController = new \controllers\UserController($db); // need path to fire it off
			$login_message = "Login required for this secure page.";
			$UserController->showAuthForm($uri,$login_message);
		}
  		else { 
    		// user is not logged in, but it's not necessary
    	//	echo"<p>Just a boring page</p>";
    		$controller = new $dispatch[0]($db); // create new controller object
			call_user_func_array(array($controller, $dispatch[1]), $matches); // fire off method with arguments
		}

    } // end matched at least one real path

        
}

ob_end_flush();



